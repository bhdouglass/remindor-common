import remindor_common.datetimeutil as d

valid_singular = [
    "now",
    "1:00pm",
    "1:00 pm",
    "13:00",
    "13",
    "1300",
    "1pm"
    ]

valid_repeating = [
    "every hour",
    "every hour from 1 to 1:00pm",
    "every minute",
    "every minute from 2:00pm to 1500",
    "every 3 minutes",
    "every 3 minutes from 3:30pm to 3:45 pm",
    "every 2 hours",
    "every 2 hours from 8 to 10"
    ]

invalid = [
    "every minute from",
    "asdf",
    "every minutes to 3",
    "2500",
    "25",
    "-1",
    "every -2 minutes",
    "every minute from 5 to 1",
    "every minute from 5 to 5",
    "8/12/13",
    "October 12",
    "7-21-2013"
    ]

print "testing valid singular times"
for row in valid_singular:
    print "?" + row + "?"
    value = d.str_time_simplify(row)
    print "!" + str(value) + "!"

    if value == None:
        print "value should not be None!"
        exit()

print ""
print "testing valid repeating times"
for row in valid_repeating:
    print "?" + row + "?"
    value = d.str_time_simplify(row)
    print "!" + str(value) + "!"

    if value == None:
        print "value should not be None!"
        exit()

print ""
print "testing invalid times"
for row in invalid:
    print row
    value = d.str_time_simplify(row)
    print value

    if value != None:
        print "value should be None!"
        exit()
