from datetime import datetime, timedelta
from dateutil import parser
import remindor_common.datetimeutil as d

singular = [
    "today",
    "tomorrow",
    "yesterday",
    "Monday",
    "mon",
    "next MON",
    "nx monday",
    "October 28, 2012",
    "oct 28",
    "9/28/2012",
    "9/28",
    "9-28-2012",
    "9-28",
    "5",
    "+5",
    "-5",
    "next 31",
    "next 1",
    "next 28"
    ]

today = datetime.today()
today = today.replace(hour=0, minute=0, second=0, microsecond=0)
valid_singular = [
    today,
    today + timedelta(days=1),
    today - timedelta(days=1),
    parser.parse("monday"),
    parser.parse("monday"),
    parser.parse("monday") + timedelta(days=7),
    parser.parse("monday") + timedelta(days=7),
    datetime(2012, 10, 28),
    datetime(2012, 10, 28),
    datetime(2012, 9, 28),
    datetime(2012, 9, 28),
    datetime(2012, 9, 28),
    datetime(2012, 9, 28),
    today + timedelta(days=5),
    today + timedelta(days=5),
    today - timedelta(days=5),
    datetime(2012, 10, 31), #set this manually
    datetime(2012, 10, 1), #set this manually
    datetime(2012, 10, 28) #set this manually
    ]

repeating = [
    "every monday",
    "every monday from 9/28/2012",
    "every monday from 2 to 30",
    "every day",
    "every day from -5",
    "every day from -5 to +5",
    "every 30",
    "every 30 from 9/28",
    "every 30 from next 1 to next 31",
    "every 30 days",
    "every 30 days from next 5",
    "every 30 days from 9/28 to next monday",
    "every weekday",
    "every weekday from monday",
    "every weekday from monday to next monday",
    "every weekend",
    "every weekend from sun",
    "every weekend from Oct 8, 2012 to October 8, 2013",
    "every other",
    "every other from 5",
    "every other from 5 to 10",
    "next 30 days",
    "next 30 days from yesterday",
    ]

#set these manually
valid_repeating = [
    "every monday",
    "every monday from 9/28/2012",
    "every monday from 2 to 30",
    "every day",
    "every day from -5",
    "every day from -5 to +5",
    "every 30",
    "every 30 from 9/28",
    "every 30 from next 1 to next 31",
    "every 30 days",
    "every 30 days from next 5",
    "every 30 days from 9/28 to next monday",
    "every weekday",
    "every weekday from monday",
    "every weekday from monday to next monday",
    "every weekend",
    "every weekend from sun",
    "every weekend from Oct 8, 2012 to October 8, 2013",
    "every other",
    "every other from 5",
    "every other from 5 to 10",
    "next 30 days",
    "next 30 days from yesterday",
    ]

counter = 0
for row in singular:
    value = d.str_date_simplify(row, 0)
    print str(value) + " (" + row + ")"
    dt = d.str_to_date(None, value)
    print dt

    if dt != valid_singular[counter]:
        print "should be " + str(valid_singular[counter])
        exit()

    counter += 1

counter = 0
for row in repeating:
    value = d.str_date_simplify(row, 0)
    print str(value) + " (" + row + ")"
    dt = d.str_to_date(None, value)
    print dt

    '''if dt != valid_singular[counter]:
        print "should be " + str(valid_singular[counter])
        exit()'''

    counter += 1
