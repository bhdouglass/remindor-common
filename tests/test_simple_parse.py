from remindor_common import datetimeutil as d

valid = [
    "exercise every 1 day at 6 am",
    "exercise every 3 days at 6 pm",
    "exercise every 3 weeks at 6 pm",
    "exercise every 3 months at 6 pm",
    "exercise every 3 years at 6 pm",
    "board meeting on 8/7/13 at 2:30 am",
    "board meeting on next monday at 2:30pm",
    "travel home on 8/7/13",
    "travel home on next monday",
    "meet bob at 6:15am",
    "meet bob at 6:15 pm",
    "take pills every 4 hours",
    "take pills every 1 minute",
    "exercise every 3 days",
    "exercise every 3 week",
    "exercise every 3 years",
    "go to the store in 20 minutes",
    "go to the store in 2 hour",
    "exercise ever 1 days at 6 pm", #actually valid because of the at
    "at 6:15pm" #actually valid without a label
    ]

invalid = [
    "exercise every x days at 6 pm",
    #"exercise ever 1 days at 6 pm", #actually valid because of the at
    "exercise every 3 days at 6x",
    "exercise",
    "exercise every at",
    "exercise at",
    "exercise every",
    "board meeting on 2:30 pm",
    "at 6:15pm meet bob",
    #"at 6:15pm", #actually valid without a label
    "go to the store in 20 days"
    ]

print "parsing valid"
for row in valid:
    print row
    parsed = d.simple_parse(row)
    print parsed

    if parsed[0] == None or parsed[1] == None:
        print "Should not be none!"
        exit()

print "\nparsing invalid"
for row in invalid:
    print row
    parsed = d.simple_parse(row)
    print parsed

    if parsed[0] != None or parsed[1] != None:
        print "Should be none!"
        exit()
