import remindor_common.datetimeutil as d

valid_singular = [
    "today",
    "tomorrow",
    "yesterday",
    "Monday",
    "mon",
    "next MON",
    "nx monday",
    "next friday",
    "October 28, 2012",
    "oct 28",
    "9/28/2012",
    "9/28",
    "9-28-2012",
    "9-28",
    "5",
    "+5",
    "-5",
    "next 31",
    "next 1",
    "next 28"
    ]

valid_repeating = [
    "every monday",
    "every monday from 9/28/2012",
    "every monday from 2 to 30",
    "every day",
    "every day from -5",
    "every day from -5 to +5",
    "every 30",
    "every 30 from 9/28",
    "every 30 from 1 to next 31",
    "every 30 days",
    "every 30 days from next 5",
    "every 30 days from 2/28 to next monday",
    "every weekday",
    "every weekday from monday",
    "every weekday from monday to next monday",
    "every weekend",
    "every weekend from sun",
    "every weekend from Oct 8, 2012 to October 8, 2013",
    "every other",
    "every other from 5",
    "every other from 5 to 10",
    "next 30 days",
    "next 30 days from yesterday",
    "every 3 weeks",
    "every 3 months",
    "every 3 years",
    "every days"
    ]

invalid = ["asdf", "532asdf", "every asdf", "nx asdf", "monday tomorrow", "tomorrow every",
    "every 30 from asdf", "every 30 asdf", "every 30 days from asdf", "every 30 from", "m",
    "every 33", "next monday from 8", "every monday to 8", "toda", "next weekday", "next day",
    "next 30 monday", "asdf 12 2012", "every other day", "every other from", "weekday next",
    "30 weekday", "next 30 monday", "every tomorrow", "every m", "next other",
    "every day from 8 to 7", "", "next 30 days from 5 to 6", "every day from to", "next -1",
    "8:30pm", "13:02", "years"]

print "testing valid singular dates"
for row in valid_singular:
    print "?" + row + "?"
    value = d.str_date_simplify(row, 0)
    print "!" + str(value) + "!"

    if value == None:
        print "value should not be None!"
        exit()

print ""
print "testing valid repeating dates"
for row in valid_repeating:
    print "?" + row + "?"
    value = d.str_date_simplify(row, 0)
    print "!" + str(value) + "!"

    if value == None:
        print "value should not be None!"
        exit()

print ""
print "testing invalid dates"
for row in invalid:
    print row
    value = d.str_date_simplify(row, 0)
    print value

    if value != None:
        print "value should be None!"
        exit()

print ''
print 'test successful'
