# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from datetime import datetime, timedelta
from os.path import basename
import logging
import optparse
import os
import sys
import json

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

import logging
logger = logging.getLogger('remindor_common')

from remindor_common.config import Config
from remindor_common import database as db, datetimeutil
from remindor_common.datetimeutil import *
from remindor_common import version, constants
from remindor_common.services.boxcar import Boxcar
from remindor_common.services.pushbullet import Pushbullet, PushbulletException

insert_values = ["+date+", "+month+", "+monthname+", "+day+", "+dayname+", "+yearday+",
                "+year+", "+time+", "+hour+", "+hour12+", "+minute+", "+second+",
                "+microsecond+", "+soundfullfile+", "+soundfile+", "+command+"]

#based on http://stackoverflow.com/a/214657
def hex_to_rgb(value):
    value = value.lstrip('#')
    length = len(value)
    rvalue = tuple(int(value[i:i + length / 3], 16) for i in range(0, length, length / 3))
    return [rvalue[0] / 256, rvalue[1] / 256, rvalue[2] / 256]

def rgb_to_hex(r, g=None, b=None, a=None):
    if g == None or b == None:
        g = r[1] * 256
        b = r[2] * 256
        a = r[3] * 256
        r = r[0] * 256

    #return '#%02x%02x%02x' % (r, g, b)
    return '#%04x%04x%04x' % (r, g, b)

def valid_time(time_s):
    return datetimeutil.str_time_simplify(time_s) != None

def valid_date(date_s, date_format):
    return datetimeutil.str_date_simplify(date_s, date_format) != None

def data_dir():
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../data/'))
    if not os.path.exists(path):
        path = os.path.join(sys.prefix, 'share/remindor-common')

    return path

def data_file(*path_parts):
    return os.path.join(data_dir(), *path_parts)

def is_number(s):
    try:
        int(s)
        return True
    except:
        return False

def is_string(s):
    return isinstance(s, basestring)

def parse_options(main_version):
    usage = _("usage: %prog [option]")
    ver = "%%prog %s (remindor-common %s)" % (main_version, version())
    description = "%s %s (remindor-common %s)" % (os.path.basename(sys.argv[0]), main_version, version())
    parser = optparse.OptionParser(version=ver, usage=usage, description=description)

    parser.add_option("-v", "--verbose", action="count", dest="verbose", help=_("Show debug messages (-vv debugs remindor_common also)"))
    parser.add_option("-a", "--add", action="store_true", dest="add", help=_("Show Add Reminder window and exit"), default=False)
    parser.add_option("-q", "--quick", action="store_true", dest="quick", help=_("Show Quick Reminder window and exit"), default=False)
    parser.add_option("-m", "--manage", action="store_true", dest="manage", help=_("Show Manage window and exit (only works if another instance is already running)"), default=False)
    parser.add_option("-p", "--preferences", action="store_true", dest="prefs", help=_("Show Preferences window and exit"), default=False)
    parser.add_option("-x", "--stop", action="store_true", dest="stop", help=_("Stops sound and exits"), default=False)
    parser.add_option("-u", "--update", action="store_true", dest="update", help=_("Forces an update of the reminder list"), default=False)
    parser.add_option("-c", "--close", action="store_true", dest="close", help=_("Requests that other instances exit."), default=False)
    parser.add_option("-s", "--simple", action="store_true", dest="simple", help=_("Show Simple Reminder window and exit"), default=False)
    #parser.add_option("-n", "--nostart", action="store_true", dest="nostart", help=_("Don't start Indicator Remindor if not running"), default=False);
    #parser.add_option("-t", "--test", action="store_true", dest="test", help=_("Runs a test and exits"), default=False);

    (options, args) = parser.parse_args()
    return options, parser

class NullHandler(logging.Handler):
    def emit(self, record):
        pass

def set_up_logging(logger_name, log, config_dir, opts, main_version):
    # add a handler to prevent basicConfig
    root = logging.getLogger()
    null_handler = NullHandler()
    root.addHandler(null_handler)

    formatter = logging.Formatter("%(asctime)s %(levelname)s:%(name)s: %(funcName)s() '%(message)s'")

    logger = logging.getLogger(logger_name)
    lib_logger = logging.getLogger('remindor_common')

    if opts.add or opts.quick or opts.manage or opts.prefs or opts.stop:
        log = config_dir + "/indicator-remindor_console.log"
    else:
        if os.path.isfile(log):
            os.remove(log) #delete old log file

    logger_fh = logging.FileHandler(log)
    logger_fh.setFormatter(formatter)
    logger.addHandler(logger_fh)
    logger.setLevel(logging.DEBUG)

    lib_logger_fh = logging.FileHandler(log)
    lib_logger_fh.setFormatter(formatter)
    lib_logger.addHandler(lib_logger_fh)
    lib_logger.setLevel(logging.DEBUG)

    # Set the logging level to show debug messages.
    if opts.verbose:
        logger_sh = logging.StreamHandler()
        logger_sh.setFormatter(formatter)
        logger.addHandler(logger_sh)
        logger.debug('verbose mode')
    if opts.verbose > 1:
        lib_logger_sh = logging.StreamHandler()
        lib_logger_sh.setFormatter(formatter)
        lib_logger.addHandler(lib_logger_sh)
        lib_logger.debug('extra verbose mode')

    logger.debug('%s version %s' % (os.path.basename(sys.argv[0]), main_version))
    logger.debug('remindor-common version ' + version())

class PushbulletInfo():
    pushbullet_delete = 'pushbullet delete'
    pushbullet_add = 'pushbullet add'
    pushbullet_invalid = 'pushbullet invalid'
    pushbullet_error = 'pushbullet error'
    pushbullet_retry = 'pushbullet retry'

    pushbullet_devices_updated = False

    def get_pushbullet_index(self, default = None):
        if default == None:
            default = self.settings.pushbullet_device

        index = 0
        if self.settings.pushbullet_api_key != '' and default != '':
            count = 1
            for device in self.settings.pushbullet_devices:
                if str(device['id']) == str(default):
                    index = count

                count += 1

        return index

    def get_pushbullet_id(self, index, devices):
        value = ''
        if devices:
            value = devices[(index - 1)]['id']
            if index == 0:
                value = ''

        return value

    def refresh_pushbullet_devices(self, api_key):
        value = self.pushbullet_error

        if api_key == '':
            self.settings.pushbullet_devices = []
            value = self.pushbullet_delete
        else:
            p = Pushbullet(api_key)
            devices = None
            status = 200
            try:
                devices =  p.devices()
            except PushbulletException as e:
                devices = None
                status = e.status

            if status == 200:
                self.settings.pushbullet_devices = devices
                self.pushbullet_devices = devices
                self.pushbullet_devices_updated = True
                #devices.insert(0, {'id': -1, 'name': 'None'}) #Do this in the gui code
                self.pushbullet_device_index = self.get_pushbullet_index()

                value = devices
            elif status == 401 or status == 403:
                value = self.pushbullet_invalid
            elif status >= 500 and status < 600:
                value = self.pushbullet_retry
            else:
                value = self.pushbullet_error

        return value

class QuickDialogInfo():
    def __init__(self, file):
        self.file = file
        self.settings = Config()

        self.show_slider = self.settings.quick_slider
        self.label = self.settings.quick_label
        self.minutes = self.settings.quick_minutes
        self.show_info = self.settings.quick_info
        self.unit = self.settings.quick_unit

        popup = _("No")
        if self.settings.quick_popup:
            popup = _("Yes")

        dialog = _("No")
        if self.settings.quick_dialog:
            dialog = _("Yes")

        sound_file = _("None")
        if self.settings.quick_sound:
            sound_file = self.settings.sound_file

            if sound_file == None or sound_file == "":
                sound_file = _("None")
            else:
                sound_file = basename(sound_file)
                sound_file += "\n" + _("(Using standard reminder sound settings)")

        self.info = _("Popup: %s\nDialog: %s\nSound File: %s") % (popup, dialog, sound_file)

    def reminder(self, label, minutes, index):
        now = datetime.now()

        if index == 1:
            now = now + timedelta(hours=minutes)
        elif index == 2:
            now = now + timedelta(days=minutes)
        else:
            now = now + timedelta(minutes=minutes)

        date_s = now.strftime(datetimeutil.internal_date_format)
        time_s = now.strftime(datetimeutil.internal_time_format)

        sound_file = ""
        sound_length = 0
        sound_loop = False
        if self.settings.quick_sound:
            sound_file = self.settings.sound_file
            sound_length = self.settings.sound_play_length
            sound_loop = self.settings.sound_loop

        a = db.Alarm(-1)
        a.set_data(label, "", time_s, date_s, sound_file, sound_length, sound_loop, \
                self.settings.quick_popup, "", False, self.settings.quick_dialog, '')

        self.database = db.Database(self.file)
        id = self.database.insert_alarm(a)
        self.database.close()

        return id

class SimpleDialogInfo():
    def __init__(self, file):
        self.file = file
        self.settings = Config()

        self.date_format = self.settings.date_format

    def validate(self, string):
        (date_s, time_s, label) = simple_parse(string, self.date_format)

        return ((date_s != None and time_s != None), date_s, time_s, label)

    def reminder(self, string):
        (valid, date_s, time_s, label) = self.validate(string)
        id = None
        if valid:
            sound_file = ""
            sound_length = 0
            sound_loop = False
            if self.settings.simple_sound:
                sound_file = self.settings.sound_file
                sound_length = self.settings.sound_play_length
                sound_loop = self.settings.sound_loop_times

            r = db.Alarm(-1)
            r.set_data(label, "", time_s, date_s, sound_file, sound_length, sound_loop, \
                    self.settings.simple_popup, "", False, self.settings.simple_dialog, '')

            self.database = db.Database(self.file)
            id = self.database.insert_alarm(r)
            self.database.close()

        return id

class ManageWindowInfo():
    today = 0
    future = 1
    past = 2

    scheduled_format = '%Y-%m-%d %H:%M:%S'

    def __init__(self, file):
        self.file = file

        self.database = db.Database(self.file)
        self.result_list = self.database.all_alarms()
        self.database.close()

        self.settings = Config()
        self.new_news = self.settings.new_news
        self.settings.new_news = False
        self.settings.dump()

        if self.new_news == None:
            self.new_news = False
        else:
            self.new_news = bool(self.new_news)

        self.hide_indicator = self.settings.hide_indicator
        self.indicator_icon = self.settings.indicator_icon
        self.show_news = self.settings.show_news

        self.missed_reminders = []
        self.potentially_missed_reminders = {}

        try:
            scheduled_reminders = json.load(open(constants.scheduled_file))
            for reminder in scheduled_reminders:
                self.potentially_missed_reminders[reminder['id']] = datetime.strptime(reminder['date_time'], self.scheduled_format)
        except:
            logger.error('Could not load scheduled reminders')

        self.update(None, True)

    def postpone(self, id):
        self.database = db.Database(self.file)
        self.settings.load()
        alarm = self.database.alarm(id)

        message = False
        if int(alarm.id) == int(id):
            if is_time_repeating(alarm.time):
                message = True
            else:
                d = str_to_date(alarm.time, alarm.date) + timedelta(minutes=self.settings.postpone)
                alarm.time = d.strftime(internal_time_format)
                if not is_date_repeating(alarm.date):
                    alarm.date = d.strftime(internal_date_format)

                self.database.insert_alarm(alarm)
                self.database.delete_alarm(id)

        self.database.close()
        return message

    def delete(self, id):
        self.database = db.Database(self.file)
        self.database.delete_alarm(id)
        self.database.close()

    def update(self, scheduler, check_missing=False):
        self.database = db.Database(self.file)
        self.result_list = self.database.all_alarms()
        self.database.close()

        self.settings.load()
        self.new_news = self.settings.new_news
        self.settings.new_news = False
        self.settings.dump()

        if self.new_news == None:
            self.new_news = False
        else:
            self.new_news = bool(self.new_news)

        self.hide_indicator = self.settings.hide_indicator
        self.indicator_icon = self.settings.indicator_icon
        self.show_news = self.settings.show_news

        self.today_color = hex_to_rgb(self.settings.today_color)
        self.future_color = hex_to_rgb(self.settings.future_color)
        self.past_color = hex_to_rgb(self.settings.past_color)

        self.today_color_hex = self.settings.today_color
        self.future_color_hex = self.settings.future_color
        self.past_color_hex = self.settings.past_color

        self.reminder_list = []
        self.scheduled_reminders = []
        for a in self.result_list:
            dt = str_to_date(a.time, a.date)
            t = timestamp(dt)
            d = str_to_date(None, a.date)
            compare_date = str_to_date(a.time, a.date, 1).date() #check returns when reminder runs based on tomorrows date

            if scheduler != None:
                scheduler.add_reminder(a.id)

            notify = _("Yes")
            if not a.notification:
                notify = _("No")

            dialog = _("Yes")
            if not a.dialog:
                dialog = _("No")

            boxcar_notify = _("Yes")
            if not a.boxcar:
                boxcar_notify = _("No")

            pushbullet_device = 'None'
            if a.pushbullet_device != '':
                for device in self.settings.pushbullet_devices:
                    if str(device['id']) == str(a.pushbullet_device):
                        pushbullet_device = device['name']

            note = a.notes.replace("\n", "/ ")
            if len(a.notes) > 50:
                note = a.notes[:50] + "..."

            sound = _("None")
            if a.sound_file != "" and a.sound_file != None:
                sound = basename(a.sound_file)

            command = a.command
            if a.command == "" or a.command == None:
                command = _("None")

            tooltip = "%s: %s\n%s: %s\n%s: %s\nBoxcar: %s\nPushbullet: %s\n%s: %s" % (_("Sound"), sound, _("Notification"), notify, _("Dialog"), dialog, boxcar_notify, pushbullet_device, _("Command"), command)

            time_s2 = fix_time_format(a.time, self.settings.time_format)
            date_s2 = fix_date_format(a.date, self.settings.date_format)

            parent = None
            today_date = date.today()
            now_datetime = datetime.now()
            now_datetime.replace(microsecond=0)

            if dt >= now_datetime and d.date() == today_date:
                parent = self.today
            elif compare_date > today_date:
                parent = self.future
            else:
                parent = self.past

            self.reminder_list.append(ReminderInfo(a.id, a.label, time_s2, date_s2, note, tooltip, parent))

            if dt > now_datetime:
                self.scheduled_reminders.append({
                    'id': a.id,
                    'date_time': dt.strftime(self.scheduled_format)
                })

            if check_missing and a.id in self.potentially_missed_reminders:
                if dt >= self.potentially_missed_reminders[a.id] and self.potentially_missed_reminders[a.id] < now_datetime:
                    self.missed_reminders.append(a.id)

        try:
            json.dump(self.scheduled_reminders, open(constants.scheduled_file, 'w'))
        except:
            logger.error('Could not save scheduled reminders')

class ReminderInfo():
    def __init__(self, id, label, time, date, note, tooltip, parent):
        self.id = str(id)
        self.label = label
        self.time = time
        self.date = date
        self.note = note
        self.tooltip = tooltip
        self.parent = parent

    def qt(self):
        return [self.label, self.time, self.date, self.note, self.id]

class ReminderDialogInfo(PushbulletInfo):
    ok = 0
    time_error = 1
    date_error = 2
    file_error = 3
    notify_warn = 4

    def __init__(self, filename):
        self.filename = filename

        self.database = db.Database(self.filename)
        self.database.close()

        self.settings = Config()

        self.label = self.settings.label
        self.time = datetimeutil.time_to_local(self.settings.time)
        self.date = datetimeutil.date_to_local(self.settings.date)
        self.command = self.settings.command
        self.notes = ""
        self.popup = self.settings.popup
        self.dialog = self.settings.dialog
        self.boxcar = self.settings.boxcar_notify
        self.boxcar_ok = self.settings.boxcar_token != ''
        self.pushbullet_api_key = self.settings.pushbullet_api_key
        self.pushbullet_device = self.settings.pushbullet_device
        self.pushbullet_devices = self.settings.pushbullet_devices
        self.pushbullet_ok = self.settings.pushbullet_api_key != ''
        self.pushbullet_device_index = self.get_pushbullet_index()
        self.sound_file = self.settings.sound_file
        self.sound_length = self.settings.sound_play_length
        self.sound_loop = self.settings.sound_loop
        self.sound_loop_times = self.settings.sound_loop_times

        self.time_format = self.settings.time_format
        self.date_format = self.settings.date_format

        self.play_sound = False
        if self.sound_file is not None and not self.sound_file == "":
            self.play_sound = True

    def reminder(self, label, time, date, command, notes, popup, dialog, boxcar, play,
                sound_file, sound_length, sound_loop, pushbullet_device, delete_id, check_warning = False):
        time = datetimeutil.str_time_simplify(time)
        if time == None:
            return (self.time_error, -1)

        date = datetimeutil.str_date_simplify(date, self.settings.date_format)
        if date == None:
            return (self.date_error, -1)

        if play:
            if sound_file == None:
                return (self.file_error, -1)
            else:
                try:
                    with open(sound_file) as f: pass
                except IOError as e:
                    return (self.file_error, -1)

        if label == "" and notes == "" and check_warning and (boxcar or popup or dialog or pushbullet_device):
            return (self.notify_warn, -1)

        if not play:
            sound_file = ""
        if sound_loop:
            sound_length = 0

        r = db.Alarm(-1)
        r.set_data(label, notes, time, date, sound_file, sound_length, sound_loop,
                popup, command, boxcar, dialog, pushbullet_device)

        self.database = db.Database(self.filename)
        id = self.database.insert_alarm(r)

        if delete_id != -1:
            self.database.delete_alarm(delete_id)

        self.database.close()

        if self.pushbullet_devices_updated:
            self.settings.dump()

        return (self.ok, id)

class TimeDialogInfo():
    no_error = 0
    once_error = 1
    from_error = 2
    to_error = 3
    order_error = 4
    from_to_error = from_error + to_error

    once = 0
    minutes = 1
    hours = 2

    active = 0
    once_s = ""
    every = 1
    check = False
    from_s = ""
    to_s = ""

    def __init__(self, time_s, filename):
        self.filename = filename

        self.settings = Config()

        self.time_format = time_formats[self.settings.time_format]
        self.qt_time_format = qt_time_formats[self.settings.time_format]
        self.time_format_num = self.settings.time_format

        self.time_s = fix_time_format(str_time_simplify(time_s), self.time_format_num) #parse time
        if self.time_s == None:
            self.time_s = ""

        if self.time_s != "":
            time_list = time_s.split(' ')
            if len(time_list) == 1:
                self.active = self.once
                self.once_s = self.time_s
            else:
                self.every = int(time_list[1])

                if time_list[2] == "minutes":
                    self.active = self.minutes
                else: #hours
                    self.active = self.hours

                if len(time_list) > 3:
                    self.from_s = time_list[4]
                    self.to_s = time_list[6]
                    self.check = True

    def validate_time(self, index, once_s, from_s, to_s):
        error_no = self.no_error
        error = False

        if index == 0:
            if simplify_singular_time(once_s) == None:
                error_no = self.once_error
        else:
            if from_s != "" and simplify_singular_time(from_s) == None:
                error_no = self.from_error

            if to_s != "" and simplify_singular_time(to_s) == None:
                error_no = error_no + self.to_error

            if not self.validate_from_to(from_s, to_s):
                error_no = self.order_error

        return error_no

    def validate_from_to(self, from_s, to_s):
        #from_dt = time_str_to_datetime(from_s, self.time_format_num)
        from_dt = simplify_singular_time(from_s)
        #to_dt = time_str_to_datetime(to_s, self.time_format_num)
        to_dt = simplify_singular_time(to_s)

        value = True
        if from_dt > to_dt:
            value = False

        if from_s == "" and to_s == "":
            value = True

        return value

    def build_time(self, index, once_s, every, from_s, to_s):
        value = ""
        if index == 0:
            value = once_s
        elif index == 1 or index == 2:
            value = "every "
            value += str(int(every)) #int() to get rid of possible double

            if index == 1:
                value += " minutes"
            else: #elif index == 2:
                value += " hours"

            if from_s != "" and to_s != "":
                value += " from " + from_s
                value += " to " + to_s

        return datetimeutil.time_to_local(value)

class DateDialogInfo():
    once = 0
    every_day = 1
    every_days = 2
    every_month = 3
    every = 4
    every_other = 5
    next_days = 6

    once_combo = ["", "today", "tomorrow", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "christmas"]
    every_combo = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "weekday", "weekend"]

    active = 0
    once_active = 0
    once_date = ""
    every_active = 0
    every_spin = 1
    check = False
    from_date = ""
    to_date = ""

    no_error = 0
    once_error = 1
    from_error = 2
    to_error = 3
    order_error = 4
    from_to_error = from_error + to_error

    def __init__(self, date_s, filename):
        self.filename = filename

        self.settings = Config()

        self.date_format = date_formats[self.settings.date_format]
        self.qt_date_format = qt_date_formats[self.settings.date_format]
        self.date_format_num = self.settings.date_format

        self.today = datetime.now()
        self.tomorrow = self.today + timedelta(days=1)

        self.once_date = self.today.strftime(self.date_format)
        self.from_date = self.today.strftime(self.date_format)
        self.to_date = self.tomorrow.strftime(self.date_format)

        self.date_s = fix_date_format(str_date_simplify(date_s, self.settings.date_format), self.settings.date_format)
        if self.date_s == None:
            self.date_s = ""

        if self.date_s != "":
            date_list = date_s.split(' ')
            if len(date_list) == 1: #once
                self.active = 0
                self.once_active = 0
                self.once_date = date_s
            elif date_list[0] == "every" and date_list[1] == "day": #every day
                self.active = 1
            elif len(date_list) > 2 and date_list[0] == "every" and date_list[2] == "days": #every X days
                self.active = 2
                self.every_spin = int(date_list[1])
            elif date_list[0] == "every" and date_list[1].isdigit(): #every Xth of the month
                self.active = 3
                self.every_spin = int(date_list[1])
            elif date_list[0] == "every" and date_list[1] in self.every_combo: #every <day>
                self.active = 4
                self.every_active = self.every_combo.index(date_list[1])
            elif date_list[0] == "every" and date_list[1] == "other": #every other day
                self.active = 5
            elif len(date_list) > 2 and date_list[0] == "next" and date_list[2] == "days": #next X days
                self.active = 6
                self.every_spin = int(date_list[1])

            #setup from/to
            if len(date_list) > 1:
                if date_list[-2] == "to":
                    self.from_date = date_list[-3]
                    self.to_date = date_list[-1]
                    self.check = True
                elif date_list[-2] == "from":
                    self.from_date = date_list[-1]
                    self.check = True

    def validate_date(self, index, once_index, once_s, from_s, to_s):
        if index == self.once:
            if once_index == 0 and simplify_singular_date(once_s, self.date_format_num) == None:
                    return self.once_error
        elif index == self.next_days:
            if from_s != "" and simplify_singular_date(from_s, self.date_format_num) == None:
                return self.from_error
        else:
            error = self.no_error
            from_date = simplify_singular_date(from_s, self.date_format_num)
            if from_s != "" and from_date == None:
                error = self.from_error

            if to_s != "" and from_s == "":
                error = self.from_error

            to_date = simplify_singular_date(to_s, self.date_format_num)
            if to_s != "" and to_date == None:
                error = error + self.to_error

            if error != self.no_error:
                return error

            from_date = datetime.strptime(from_date, internal_date_format)
            to_date = datetime.strptime(to_date, internal_date_format)

            if from_date > to_date:
                return self.order_error

        return self.no_error

    def validate_from_to(self, from_s, to_s):
        from_dt = date_str_to_datetime(from_s, self.date_format_num)
        to_dt = date_str_to_datetime(to_s, self.date_format_num)

        value = True
        if from_dt > to_dt:
            value = False

        return value

    def build_date(self, index, once, once_date, every, every_spin, from_date, to_date):
        value = ""
        if index == 0: #once
            if once == 0:
                value = once_date
            else:
                value = self.once_combo[once]
        elif index == 1: #every day
            value = "every day"
        elif index == 2: #every X days
            value = "every "
            value += str(int(every_spin))
            value += " days"
        elif index == 3: #every Xth of the month
            value = "every "
            value += str(int(every_spin))
        elif index == 4: #every <day>
            value = "every "
            value += self.every_combo[every]
        elif index == 5: #every other day
            value = "every other"
        elif index == 6: #Next X days
            value = "next "
            value += str(int(every_spin))
            value += " days"

        if index > 0:
            if from_date != "":
                value += " from " + from_date

            if index != 6:
                if from_date != "" and to_date != "":
                    value += " to " + to_date

        return datetimeutil.date_to_local(value.lower())

class PreferencesDialogInfo(PushbulletInfo):
    ok = 0
    time_error = 1
    date_error = 2

    boxcar_delete = 0
    boxcar_add = 1
    boxcar_subscribe = 2
    boxcar_error = 3

    def __init__(self, filename):
        self.filename = filename
        self.settings = Config()

        self.today_color_rgb = hex_to_rgb(self.settings.today_color)
        self.future_color_rgb = hex_to_rgb(self.settings.future_color)
        self.past_color_rgb = hex_to_rgb(self.settings.past_color)

        self.time_format = time_formats[self.settings.time_format]
        self.qt_time_format = qt_time_formats[self.settings.time_format]
        self.time_format_num = self.settings.time_format

        self.date_format = date_formats[self.settings.date_format]
        self.qt_date_format = qt_date_formats[self.settings.date_format]
        self.date_format_num = self.settings.date_format

        self.pushbullet_device_index = self.get_pushbullet_index()

    def preferences(self, settings):
        if not valid_time(settings.time):
            return self.time_error
        if not valid_date(settings.date, settings.date_format):
            return self.date_error

        self.settings.dump()

        return self.ok

    '''
    def validate_boxcar(self, boxcar_email, boxcar_notify, boxcar_original):
        return_status = self.boxcar_add

        if boxcar_email != boxcar_original and boxcar_email != "":
            status = Boxcar(boxcar_email).subscribe()
            if status == 404: #user not in system
                return_status = self.boxcar_subscribe
            elif status == 200 or status == 401: #ok or user already subscribed
                return_status = self.boxcar_add
            else: #other error
                logger.debug("boxcar error: " + str(status))
                return_status = self.boxcar_error
        elif boxcar_email == "":
            return_status = self.boxcar_delete

        return return_status
    '''
