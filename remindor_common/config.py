# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import json, os

from remindor_common import version, constants
from remindor_common.database import Database

class Config():
    def __init__(self, program = None, program_version = None):
        self.filename = constants.config_file
        self.database_filename = constants.database_file
        self.exists = self.load()

        if program is not None:
            self.set('program', program)
        if program_version is not None:
            self.set('version', program_version)
        self.set('remindor_common_version', version())

        if not self.exists and program is not None:
            self.import_database()

    def load(self):
        exists = True
        if os.path.exists(self.filename):
            self.dictionary = json.load(open(self.filename))
        else:
            self.dictionary = {}
            exists = False

        self.label 				= self.get('label', '')
        self.time 				= self.get('time', 'now')
        self.date 				= self.get('date', 'today')
        self.command 			= self.get('command', '')
        self.sound_file 		= self.get('sound_file', '')
        self.sound_play_length 	= self.get('sound_play_length', 10)
        self.sound_loop 		= self.get('sound_loop', False)
        self.sound_loop_times 	= self.get('sound_loop_times', 5)
        self.popup 				= self.get('popup', True)
        self.dialog 			= self.get('dialog', False)
        self.change_indicator 	= self.get('change_indicator', True)
        self.hide_indicator 	= self.get('hide_indicator', False)
        self.indicator_icon 	= self.get('indicator_icon', 0)
        self.postpone 			= self.get('postpone', 30)

        self.time_format 		= self.get('time_format', 0)
        self.date_format 		= self.get('date_format', 0)
        self.today_color 		= self.get('today_color', '#00007878ffff')
        self.future_color 		= self.get('future_color', '#04039e9e0000')
        self.past_color 		= self.get('past_color', '#ffffecec1f1f')
        self.show_news 			= self.get('show_news', True)

        self.quick_label 		= self.get('quick_label', '')
        self.quick_minutes 		= self.get('quick_minutes', 30)
        self.quick_popup 		= self.get('quick_popup', True)
        self.quick_dialog 		= self.get('quick_dialog', False)
        self.quick_sound 		= self.get('quick_sound', False)
        self.quick_info 		= self.get('quick_info', True)
        self.quick_slider 		= self.get('quick_slider', True)
        self.quick_unit 		= self.get('quick_unit', 0)

        self.simple_popup 		= self.get('simple_popup', True)
        self.simple_dialog 		= self.get('simple_dialog', False)
        self.simple_sound 		= self.get('simple_sound', False)

        self.boxcar_token		= self.get('boxcar_token', '')
        self.boxcar_notify		= self.get('boxcar_notify', 0)

        self.pushbullet_api_key = self.get('pushbullet_api_key', '')
        self.pushbullet_device  = self.get('pushbullet_device', '')
        self.pushbullet_devices = self.get('pushbullet_devices', [])

        self.last_news			= self.get('last_news', '')
        self.new_news			= self.get('new_news', False)

        if not exists:
            self.dump()

        return exists

    def dump(self):
        self.set('label', self.label)
        self.set('time', self.time)
        self.set('date', self.date)
        self.set('command', self.command)
        self.set('sound_file', self.sound_file)
        self.set('sound_play_length', self.sound_play_length)
        self.set('sound_loop', self.sound_loop)
        self.set('sound_loop_times', self.sound_loop_times)
        self.set('popup', self.popup)
        self.set('dialog', self.dialog)
        self.set('change_indicator', self.change_indicator)
        self.set('hide_indicator', self.hide_indicator)
        self.set('indicator_icon', self.indicator_icon)
        self.set('postpone', self.postpone)
        self.set('time_format', self.time_format)
        self.set('date_format', self.date_format)
        self.set('today_color', self.today_color)
        self.set('future_color', self.future_color)
        self.set('past_color', self.past_color)
        self.set('show_news', self.show_news)
        self.set('quick_label', self.quick_label)
        self.set('quick_minutes', self.quick_minutes)
        self.set('quick_popup', self.quick_popup)
        self.set('quick_dialog', self.quick_dialog)
        self.set('quick_sound', self.quick_sound)
        self.set('quick_info', self.quick_info)
        self.set('quick_slider', self.quick_slider)
        self.set('quick_unit', self.quick_unit)
        self.set('simple_popup', self.simple_popup)
        self.set('simple_dialog', self.simple_dialog)
        self.set('simple_sound', self.simple_sound)
        self.set('boxcar_token', self.boxcar_token)
        self.set('boxcar_notify', self.boxcar_notify)
        self.set('pushbullet_api_key', self.pushbullet_api_key)
        self.set('pushbullet_device', self.pushbullet_device)
        self.set('pushbullet_devices', self.pushbullet_devices)
        self.set('last_news', self.last_news)
        self.set('new_news', self.new_news)

        json.dump(self.dictionary, open(self.filename, 'w'))

    def get(self, attr, default = None):
        if attr not in self.dictionary:
            self.set(attr, default)

        return self.dictionary[attr]

    def set(self, attr, value):
        self.dictionary[attr] = value
        setattr(self, attr, value)

    def import_database(self):
        if os.path.exists(self.database_filename):
            db = Database(self.database_filename)
            if (db.has_settings):
                settings = db.settings()
                boxcar_service = db.service('boxcar')

                self.set('label', settings.label)
                self.set('time', settings.time)
                self.set('date', settings.date)
                self.set('command', settings.command)
                self.set('sound_file', settings.sound_file)
                self.set('sound_play_length', settings.sound_length)
                self.set('sound_loop', settings.sound_loop)
                self.set('sound_loop_times', settings.sound_loop_times)
                self.set('popup', settings.notification)
                self.set('dialog', settings.dialog)
                self.set('change_indicator', settings.change_icon)
                self.set('hide_indicator', settings.hide_indicator)
                self.set('indicator_icon', settings.indicator_icon)
                self.set('postpone', settings.postpone)

                self.set('time_format', settings.time_format)
                self.set('date_format', settings.date_format)
                self.set('today_color', settings.today_color)
                self.set('future_color', settings.future_color)
                self.set('past_color', settings.past_color)
                self.set('show_news', settings.show_news)

                self.set('quick_label', settings.quick_label)
                self.set('quick_minutes', settings.quick_minutes)
                self.set('quick_popup', settings.quick_popup)
                self.set('quick_dialog', settings.quick_dialog)
                self.set('quick_sound', settings.quick_sound)
                self.set('quick_info', settings.quick_info)
                self.set('quick_slider', settings.quick_slider)
                self.set('quick_unit', settings.quick_unit)

                #self.set('boxcar_token', boxcar_service.email)
                self.set('boxcar_notify', boxcar_service.default_notify)

                self.set('last_news', db.get_internal('news_flash'))

                db.delete_settings()
                self.dump()

            db.close()
