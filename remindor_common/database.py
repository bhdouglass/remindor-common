# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import sqlite3, os

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

from remindor_common import datetimeutil

#def database_file():
    #return os.getenv('HOME') + '/.config/indicator-remindor/indicator-remindor.db'

class Database():
    def __init__(self, filename):
        self.filename = filename
        directory = os.path.dirname(self.filename)
        if not os.path.exists(directory):
            os.makedirs(directory)

        migrate = False
        if not os.path.exists(filename):
            migrate = True

        self.connection = sqlite3.connect(filename)
        self.connection.text_factory = str
        self.cursor = self.connection.cursor()

        self.setup()
        if migrate:
            self.migrate_from_couch()

        if self.old_version < 9:
            self.fix_time_from_to()

        if self.old_version < 11:
            self.fix_time()

    def setup(self):
        self.execute("""CREATE TABLE IF NOT EXISTS version (id INTEGER PRIMARY KEY AUTOINCREMENT,
            version TEXT)""")
        self.old_version = self.version()
        if self.old_version < 11:
            self.version(11)

        #00 - id
        #01 - label
        #02 - notes
        #03 - time
        #04 - date
        #05 - sound
        #06 - sound length
        #07 - sound loop
        #08 - popup notification
        #09 - command
        #10 - boxcar notification
        #11 - popup dialog
        #12 - pushbullet device

        self.execute("""CREATE TABLE IF NOT EXISTS alarms (id INTEGER PRIMARY KEY AUTOINCREMENT, label TEXT,
            notes TEXT, time TEXT, date TEXT, sound TEXT, sound_length INTEGER, sound_loop INTEGER,
            notification BOOLEAN, command TEXT, boxcar_notification BOOLEAN DEFAULT 0,
            dialog BOOLEAN DEFAULT 0, pushbullet_device TEXT DEFAULT '')""")

        if self.old_version < 1:
            try:
                self.fetchone("SELECT boxcar_notification FROM alarms WHERE 1")
            except sqlite3.OperationalError:
                self.execute("ALTER TABLE alarms ADD COLUMN boxcar_notification BOOLEAN DEFAULT 0")
        if self.old_version < 2:
            try:
                self.fetchone("SELECT dialog FROM alarms WHERE 1")
            except sqlite3.OperationalError:
                self.execute("ALTER TABLE alarms ADD COLUMN dialog BOOLEAN DEFAULT 0")
        if self.old_version < 10:
            try:
                self.fetchone('SELECT pushbullet_device FROM alarms WHERE 1')
            except sqlite3.OperationalError:
                self.execute("ALTER TABLE alarms ADD COLUMN pushbullet_device TEXT DEFAULT ''")

        #00 - id
        #01 - label
        #02 - quick label
        #03 - time
        #04 - date
        #05 - command
        #06 - sound
        #07 - sound length
        #08 - sound loop
        #09 - sound loop times
        #10 - notification (popup)
        #11 - time format
        #12 - date format
        #13 - today color
        #14 - future color
        #15 - past color
        #16 - change indicator icon
        #17 - popup dialog
        #18 - postpone minutes
        #19 - quick minutes
        #20 - news notification
        #21 - quick reminder popup
        #22 - quick reminder dialog
        #23 - quick reminder use standard reminder sound
        #24 - quick show info on dialog
        #25 - hide indicator
        #26 - indicator icon index
        #27 - quick use slider
        #28 - quick time unit

        self.has_settings = (self.fetchone("SELECT name FROM sqlite_master WHERE type='table' AND name='settings'") != None)

        if self.has_settings:
            self.execute("""CREATE TABLE IF NOT EXISTS settings (id INTEGER PRIMARY KEY AUTOINCREMENT,
                label TEXT, quick_label TEXT, time TEXT, date TEXT, command TEXT, sound TEXT,
                sound_length INTEGER, sound_loop BOOLEAN, sound_loop_times INTEGER, notification BOOLEAN,
                time_format INTEGER, date_format INTEGER, today_color TEXT, future_color TEXT,
                past_color TEXT, indicator_icon BOOLEAN DEFAULT 1, dialog BOOLEAN DEFAULT 0,
                postpone INTEGER DEFAULT 30, quick_minutes INTEGER DEFAULT 30, news BOOLEAN DEFAULT 1,
                quick_popup BOOLEAN DEFAULT 1, quick_dialog BOOLEAN DEFAULT 0, quick_sound BOOLEAN DEFAULT 0,
                quick_info BOOLEAN DEFAULT 1, hide_indicator BOOLEAN DEFAULT 0,
                indicator_icon_index INTEGER DEFAULT 0, quick_slider BOOLEAN DEFAULT 1,
                quick_unit INTEGER DEFAULT 0)""")

            if self.old_version < 1:
                try:
                    self.fetchone("SELECT indicator_icon FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN indicator_icon BOOLEAN DEFAULT 1")
            if self.old_version < 2:
                try:
                    self.fetchone("SELECT dialog FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN dialog BOOLEAN DEFAULT 0")
            if self.old_version < 3:
                try:
                    self.fetchone("SELECT postpone FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN postpone INTEGER DEFAULT 30")

                try:
                    self.fetchone("SELECT quick_minutes FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN quick_minutes INTEGER DEFAULT 30")
            if self.old_version < 4:
                try:
                    self.fetchone("SELECT news FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN news BOOLEAN DEFAULT 1")
            if self.old_version < 5:
                try:
                    self.fetchone("SELECT quick_popup FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN quick_popup BOOLEAN DEFAULT 1")
                    self.execute("ALTER TABLE settings ADD COLUMN quick_dialog BOOLEAN DEFAULT 0")
                    self.execute("ALTER TABLE settings ADD COLUMN quick_sound BOOLEAN DEFAULT 0")
                    self.execute("ALTER TABLE settings ADD COLUMN quick_info BOOLEAN DEFAULT 1")
                    self.execute("ALTER TABLE settings ADD COLUMN hide_indicator BOOLEAN DEFAULT 0")
            if self.old_version < 6:
                try:
                    self.fetchone("SELECT indicator_icon_index FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN indicator_icon_index INTEGER DEFAULT 0")
            if self.old_version < 7:
                try:
                    self.fetchone("SELECT quick_slider FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN quick_slider BOOLEAN DEFAULT 1")
            if self.old_version < 8:
                try:
                    self.fetchone("SELECT quick_unit FROM settings WHERE 1")
                except sqlite3.OperationalError:
                    self.execute("ALTER TABLE settings ADD COLUMN quick_unit INTEGER DEFAULT 0")

            #00 - id
            #01 - type
            #02 - email
            #03 - secret
            #04 - default notify

            self.execute("""CREATE TABLE IF NOT EXISTS services (id INTEGER PRIMARY KEY AUTOINCREMENT,
                type TEXT, email TEXT, secret TEXT, default_notification BOOLEAN)""")

            #00 - id
            #01 - key
            #02 - value

            self.execute("""CREATE TABLE IF NOT EXISTS internal (id INTEGER PRIMARY KEY AUTOINCREMENT,
                key TEXT, value TEXT)""")

            row = self.fetchone('SELECT * FROM settings WHERE 1')
            if row == None:
                indicator_icon_index = 0
                #if os.name == 'nt':
                #    indicator_icon_index = 3

                self.execute("""INSERT INTO settings (label, quick_label, time, date, command,
                    sound, sound_length, sound_loop, sound_loop_times, notification, time_format,
                    date_format, today_color, future_color, past_color, indicator_icon, dialog,
                    postpone, quick_minutes, news, quick_popup, quick_dialog, quick_sound, quick_info,
                    hide_indicator, indicator_icon_index, quick_slider, quick_unit)
                    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                    ["", _("Quick Reminder"), _("now"), _("today"), "", "", 10, False, 5, True, 0, 0,
                    "#00007878ffff", "#04039e9e0000", "#ffffecec1f1f", True, False, 30, 30, True,
                    True, False, False, True, False, indicator_icon_index, True, 0]
                )

    def fix_time_from_to(self):
        reminder_list = self.all_alarms()
        for reminder in reminder_list:
            new_time = datetimeutil.fix_time_from_to(reminder.time)
            if reminder.time != new_time and new_time != None:
                print reminder.time
                print new_time
                reminder.time = new_time
                self.delete_alarm(reminder.id)
                self.insert_alarm(reminder)

    def fix_time(self):
        reminder_list = self.all_alarms()
        for reminder in reminder_list:
            time = datetimeutil.str_time_simplify(reminder.time)
            sql = """UPDATE alarms SET time = ?"""
            self.execute(sql, [time])

    def migrate_from_couch(self):
        try:
            from desktopcouch.records.server import CouchDatabase
            from desktopcouch.records.record import Record
            imported = True
        except ImportError:
            return

        db = 0
        try:
            db = CouchDatabase('indicator-remindor-alarms', create=False)
        except:
            return

        if not db.view_exists("list", "app"):
            map_js = """function(doc) { emit(doc._id, doc) }"""
            db.add_view("list", map_js, None, "app")

        result = db.execute_view("list", "app")
        result_list = list(result)

        for row in result_list:
            time_s = datetimeutil.format_time(row.value["hour"], row.value["minute"], row.value["am_pm"])
            sound_length = 0
            sound_loop = False

            try:
                sound_length = row.value["sound_length"]
            except KeyError:
                sound_length = 0

            try:
                sound_loop = row.value["sound_loop"]
            except KeyError:
                sound_loop = False

            self.execute("""INSERT INTO alarms (label, notes, time, date, sound, sound_length,
                sound_loop, notification, command) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                (row.value["label"], row.value["notes"], time_s, row.value["date"],
                row.value["sound"], sound_length, sound_loop,
                row.value["notification"], row.value["command"]))

    def delete_settings(self):
        self.execute('DROP TABLE settings')
        self.execute('DROP TABLE services')
        self.execute('DROP TABLE internal')
        self.has_settings = False

    def version(self, v=None):
        if v == None:
            row = self.fetchone("SELECT * FROM version WHERE 1")

            v = 0
            if row is not None:
                try:
                    v = int(row[1])
                except:
                    pass

            return v
        else:
            self.execute("DELETE FROM version WHERE 1")
            self.execute("INSERT INTO version (version) VALUES(?)", [str(v)])

    def execute(self, sql, data=None):
        if data == None:
            self.cursor.execute(sql)
        else:
            self.cursor.execute(sql, data)

        sql_list = sql.split(' ')
        if sql_list[0] != "SELECT":
            self.connection.commit()

    def fetchone(self, sql, data=None):
        self.execute(sql, data)
        return self.cursor.fetchone()

    def fetchall(self, sql, data=None):
        self.execute(sql, data)
        return self.cursor.fetchall()

    def close(self):
        self.connection.close()

    def alarm(self, id):
        row = self.fetchone("SELECT * FROM alarms WHERE id = ?", [str(id)])
        a = Alarm(-1)
        a.set_data_row(row)
        return a

    def all_alarms(self):
        result_list = self.fetchall("SELECT * FROM alarms")
        alarm_list = list()
        for row in result_list:
            a = Alarm(-1)
            a.set_data_row(row)
            alarm_list.append(a)

        return alarm_list

    def insert_alarm(self, a):
        sql = """INSERT INTO alarms (label, notes, time, date, sound,
                sound_length, sound_loop, notification, command, boxcar_notification,
                dialog, pushbullet_device) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
        data = a.get_data_row()
        self.execute(sql, data)
        return self.cursor.lastrowid

    def delete_alarm(self, id):
        self.execute("DELETE FROM alarms WHERE id = ?", [str(id)])

    def settings(self):
        if not self.has_settings:
            raise Exception('No settings to load!')

        row = self.fetchone("SELECT * FROM settings WHERE 1")
        return Settings(row)

    def set_settings(self, s):
        if not self.has_settings:
            raise Exception('No settings to set!')

        self.execute("DELETE FROM settings WHERE 1")

        sql = """INSERT INTO settings (label, quick_label, time, date, command,
            sound, sound_length, sound_loop, sound_loop_times, notification, time_format,
            date_format, today_color, future_color, past_color, indicator_icon, dialog,
            postpone, quick_minutes, news, quick_popup, quick_dialog, quick_sound, quick_info,
            hide_indicator, indicator_icon_index, quick_slider, quick_unit)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
        data = s.get_data_row()
        self.execute(sql, data)

        return self.cursor.lastrowid

    def services(self):
        if not self.has_settings:
            raise Exception('No services to load!')

        rows = self.fetchall("SELECT * FROM services WHERE 1")
        return Services(rows)

    def service(self, type):
        if not self.has_settings:
            raise Exception('No services to load!')

        s = self.services()
        return s.service(type)

    def set_service(self, s):
        if not self.has_settings:
            raise Exception('No services to set!!')

        self.remove_service(s.type)

        sql = "INSERT INTO services (type, email, secret, default_notification) VALUES(?, ?, ?, ?)"
        data = s.get_data_row()
        self.execute(sql, data)

        return self.cursor.lastrowid

    def remove_service(self, type):
        if not self.has_settings:
            raise Exception('No services to remove!')

        self.execute("DELETE FROM services WHERE type = ?", [type])

    def get_internal(self, key):
        if not self.has_settings:
            raise Exception('No internals to load!')

        internal = self.fetchone("SELECT * FROM internal WHERE key = ?", [key])

        if internal != None:
            value = internal[2]
        else:
            value = None

        return value

    def set_internal(self, key, value):
        if not self.has_settings:
            raise Exception('No internals to set!')

        self.remove_internal(key)
        self.execute("INSERT INTO internal (key, value) VALUES(?, ?)", [key, value])

        return self.cursor.lastrowid

    def remove_internal(self, key):
        self.execute("DELETE FROM internal WHERE key = ?", [key])

class Alarm():
    def __init__(self, id):
        self.id = id
        self.label = ""
        self.notes = ""
        self.time = "now"
        self.date = "today"
        self.sound_file = ""
        self.sound_length = 0
        self.sound_loop = True
        self.notification = True
        self.command = ""
        self.boxcar = False
        self.dialog = False
        self.pushbullet_device = ''

    def set_data_row(self, row):
        if row != None:
            self.id = row[0]
            self.label = row[1]
            self.notes = row[2]
            self.time = row[3]
            self.date = row[4]
            self.sound_file = row[5]
            self.sound_length = row[6]
            self.sound_loop = row[7]
            self.notification = row[8]
            self.command = row[9]
            self.boxcar = row[10]
            self.dialog = row[11]
            self.pushbullet_device = row[12]

    def set_data(self, label, notes, time, date, sound_file, sound_length, sound_loop,
            notification, command, boxcar, dialog, pushbullet_device):
        self.label = label
        self.notes = notes
        self.time = time
        self.date = date
        self.sound_file = sound_file
        self.sound_length = sound_length
        self.sound_loop = sound_loop
        self.notification = notification
        self.command = command
        self.boxcar = boxcar
        self.dialog = dialog
        self.pushbullet_device = pushbullet_device

    def get_data_row(self):
        row = list()
        #row[0] = self.id
        row.append(self.label)
        row.append(self.notes)
        row.append(self.time)
        row.append(self.date)
        row.append(self.sound_file)
        row.append(self.sound_length)
        row.append(self.sound_loop)
        row.append(self.notification)
        row.append(self.command)
        row.append(self.boxcar)
        row.append(self.dialog)
        row.append(self.pushbullet_device)

        return row

    def get_data(self):
        return (self.id, self.label, self.notes, self.time, self.date, self.sound_file,
            self.sound_length, self.sound_loop, self.notification, self.command,
            self.boxcar, self.dialog, self.pushbullet_device)

class Settings():
    def __init__(self, row):
        self.id = row[0]
        self.label = row[1]
        self.quick_label = row[2]
        self.time = row[3]
        self.date = row[4]
        self.command = row[5]
        self.sound_file = row[6]
        self.sound_length = row[7]
        self.sound_loop = row[8]
        self.sound_loop_times = row[9]
        self.notification = row[10]
        self.time_format = row[11]
        self.date_format = row[12]
        self.today_color = row[13]
        self.future_color = row[14]
        self.past_color = row[15]
        self.change_icon = row[16]
        self.dialog = row[17]
        self.postpone = row[18]
        self.quick_minutes = row[19]
        self.show_news = row[20]
        self.quick_popup = row[21]
        self.quick_dialog = row[22]
        self.quick_sound = row[23]
        self.quick_info = row[24]
        self.hide_indicator = row[25]
        self.indicator_icon = row[26]
        self.quick_slider = row[27]
        self.quick_unit = row[28]

    def get_data_row(self):
        row = list()
        row.append(self.label)
        row.append(self.quick_label)
        row.append(self.time)
        row.append(self.date)
        row.append(self.command)
        row.append(self.sound_file)
        row.append(self.sound_length)
        row.append(self.sound_loop)
        row.append(self.sound_loop_times)
        row.append(self.notification)
        row.append(self.time_format)
        row.append(self.date_format)
        row.append(self.today_color)
        row.append(self.future_color)
        row.append(self.past_color)
        row.append(self.change_icon)
        row.append(self.dialog)
        row.append(self.postpone)
        row.append(self.quick_minutes)
        row.append(self.show_news)
        row.append(self.quick_popup)
        row.append(self.quick_dialog)
        row.append(self.quick_sound)
        row.append(self.quick_info)
        row.append(self.hide_indicator)
        row.append(self.indicator_icon)
        row.append(self.quick_slider)
        row.append(self.quick_unit)

        return row

class Services():
    def __init__(self, rows):
        self.dictionary = dict()
        for row in rows:
            self.dictionary[row[1]] = Service(row[1], row[2], row[3], row[4])

    def service(self, type):
        try:
            return self.dictionary[type]
        except KeyError:
            return Service(type, "", "", False)

class Service():
    def __init__(self, type, email, secret, default_notify):
        self.type = type
        self.email = email
        self.secret = secret
        self.default_notify = default_notify

    def get_data_row(self):
        row = list()
        row.append(self.type)
        row.append(self.email)
        row.append(self.secret)
        row.append(self.default_notify)

        return row
