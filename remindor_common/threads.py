# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from datetime import datetime
import time
import threading
import sys
import feedparser
import requests
import logging
import subprocess
import os
logger = logging.getLogger('remindor_common')

from remindor_common import database as db
from remindor_common.datetimeutil import internal_date_format, internal_time_format

news_format = internal_date_format + " " + internal_time_format + ":%S"

class BlogReader(threading.Thread):
    def __init__(self, rssfeed, database_file):
        threading.Thread.__init__(self)
        self.rssfeed = rssfeed
        self.database_file = database_file

    def run(self):
        logger.debug("checking for news")
        try:
            rss = feedparser.parse(self.rssfeed)
            if rss.status == 200:
                u = rss.entries[0].updated_parsed
                updated = datetime(u.tm_year, u.tm_mon, u.tm_mday, u.tm_hour, u.tm_min, u.tm_sec)
                updated_s = updated.strftime(news_format)

                database = db.Database(self.database_file)
                news = database.get_internal("news_flash")

                update_btn = False
                if news == None:
                    database.set_internal("news_flash", updated_s)
                    update_btn = True
                else:
                    news = datetime.strptime(news, news_format)
                    if updated > news:
                        database.set_internal("news_flash", updated_s)
                        update_btn = True

                logger.debug("update: " + str(update_btn))
                if update_btn:
                    database.set_internal("new_news", "1")
                else:
                    database.set_internal("new_news", "0")

                database.close()
            else:
                logger.debug("non-normal http status: " + str(rss.status))
        except:
            logger.warning

        logger.debug("done checking for news")

class PostRequest(threading.Thread):
    def __init__(self, url, data):
        threading.Thread.__init__(self)
        self.data = data
        self.url = url

    def run(self):
        status = -1
        logger.debug("sending post request to: " + self.url)
        try:
            r = requests.post(self.url, params=self.data)
            logger.debug("Post request content: " + r.content)
            status = r.status_code
        except:
            logger.warning("Exception caught trying post request: " + str(sys.exc_info()[0]))

        logger.debug("Post request status: " + str(status))

class RunCommand(threading.Thread):
    def __init__(self, command, config_dir):
        threading.Thread.__init__(self)
        self.command = command
        self.outfile = os.path.join(config_dir, 'command_output.txt')

    def run(self):
        logger.debug('start command: %s' % (self.command))
        start = time.time()

        output = ''
        try:
            output = subprocess.check_output(self.command, stderr=subprocess.STDOUT, shell=True)
            logger.debug('command exited with a status of 0')
        except subprocess.CalledProcessError as e:
            logger.error('command exited with a status of %d' % (e.returncode))
            output = e.output

        try:
            f = open(self.outfile, 'w')
            f.write(output)
            f.close()
        except IOError:
            logger.error('could not write output to the file: %s' % (self.outfile))
            logger.debug('command output: %s' % (output))

        logger.debug('end command (%ss)' % (time.time() - start))
