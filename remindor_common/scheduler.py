# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from remindor_common.config import Config
from remindor_common.database import Database
from remindor_common import services
from remindor_common import database as db
from remindor_common.datetimeutil import timestamp, format_time, str_to_date, is_time_repeating, format_time3, is_date_repeating, fix_time_format, fix_date_format, internal_date_format, internal_time_format, time_formats, date_formats
from remindor_common.threads import RunCommand
from remindor_common.services.boxcar import Boxcar
from remindor_common.services.pushbullet import Pushbullet

import subprocess, shlex
from os.path import basename
from datetime import timedelta, date, datetime
import sys

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

import logging
logger = logging.getLogger('remindor_common')

class GenericScheduler():
    def __init__(self, db_file, config_dir):
        logger.debug("initializing genericscheduler")
        self.schedule = []
        self.playing_sound = None
        self.dbus_service = None
        self.sound_loop_times = 0
        self.db_file = db_file
        self.config_dir = config_dir

    def add_dbus_service(self, dbus_service):
        self.dbus_service = dbus_service

    def clear_schedule(self):
        logger.debug("scheduler: clear_schedule")
        for reminder in self.schedule:
            self.remove_reminder(reminder)

        self.schedule = []

    def remove_reminder(self, reminder):
        raise NotImplementedError("Need to implement remove_reminder!")

    def change_icon(self):
        raise NotImplementedError("Need to implement change_icon!")

    def run_command(self, command):
        logger.debug("scheduler: run_command")

        rc = RunCommand(command, self.config_dir)
        rc.run()

        logger.debug("scheduler: done run_command")

    def play_sound(self, sound_file, sound_loop, sound_length):
        logger.debug("scheduler: play_sound")
        logger.debug("playing: " + sound_file)
        logger.debug("loop: " + str(sound_loop))
        logger.debug("length: " + str(sound_length))

        loop = 0
        if sound_loop:
            loop = Config().sound_loop_times

        if self.playing_sound != None:
            self.remove_playing_sound()
            self.playing_sound = None

        self.play_sound_helper(sound_file, sound_loop, loop)

        if int(sound_length) > 0 and loop == 0:
            self.add_playing_sound(sound_length)

        logger.debug("scheduler: done play_sound")

    def play_sound_helper(self, sound_file, sound_loop, sound_loop_times):
        raise NotImplementedError("Need to implement play_sound_helper!")

    def stop_sound(self, widget=None, data=None):
        logger.debug("scheduler: stop_sound")
        if self.playing_sound != None:
            self.remove_playing_sound()
            self.playing_sound = None

        self.stop_sound_helper()

    def stop_sound_helper(self):
        raise NotImplementedError("Need to implement stop_sound_helper!")

    def remove_playing_sound(self):
        raise NotImplementedError("Need to implement remove_playing_sound!")

    def add_playing_sound(self, length):
        raise NotImplementedError("Need to implement add_playing_sound!")

    def popup_notification(self, label, notes):
        raise NotImplementedError("Need to implement popup_notification!")

    def popup_dialog(self, label, notes):
        raise NotImplementedError("Need to implement popup_dialog!")

    def run_boxcar(self, boxcar_token, label, notes):
        logger.debug("scheduler: run_boxcar")
        Boxcar(boxcar_token).notify(label, notes)

    def run_pushbullet(self, pushbullet_api_key, pushbullet_device, label, notes):
        logger.debug("scheduler: run_pushbullet")
        status = Pushbullet(pushbullet_api_key).note(pushbullet_device, label, notes)

    def run_alarm(self, id):
        logger.debug("scheduler: run_alarm: " + str(id))
        database = db.Database(self.db_file)
        a = database.alarm(id)
        database.close()

        settings = Config()
        change_icon = settings.change_indicator
        self.sound_loop_times = settings.sound_loop_times

        if int(a.id) == int(id):
            logger.debug("run_alarm: " + a.label)

            label = a.label
            if label == "":
                label = _("Reminder")

            command = self.expand_inserts(a.command, settings, a.command, a.sound_file)
            notes = self.expand_inserts(a.notes, settings, command, a.sound_file)
            if notes == "" and a.label != "":
                notes = label

            if change_icon:
                self.change_icon()

            if command != "":
                self.run_command(command)

            if a.sound_file != "" and a.sound_file != None:
                self.play_sound(a.sound_file, a.sound_loop, a.sound_length)

            if a.notification:
                self.popup_notification(label, notes)

            if a.dialog:
                self.popup_dialog(label, notes)

            if a.boxcar and settings.boxcar_token != "":
                self.run_boxcar(settings.boxcar_token, a.label, notes)

            if a.pushbullet_device and settings.pushbullet_api_key:
                self.run_pushbullet(settings.pushbullet_api_key, a.pushbullet_device, a.label, notes)

        self.update_schedule()
        logger.debug("scheduler: done run_alarm")

    def update_schedule(self):
        raise NotImplementedError("Need to implement update_schedule!")

    def add_to_schedule(self, delay, id):
        raise NotImplementedError("Need to implement add_to_schedule!")

    def add_reminder(self, id):
        logger.debug("scheduler: add_reminder")
        database = db.Database(self.db_file)
        a = database.alarm(id)
        database.close()

        d = str_to_date(a.time, a.date)
        logger.debug("scheduler: add_reminder, datetime: " + str(d.strftime(internal_date_format + " " + internal_time_format)) + " (" + a.date + " " + a.time + ")")
        delay = timestamp(d) - timestamp(datetime.now())

        if delay > 0:
            self.add_to_schedule(delay, id)
            logger.debug("timer for " + str(delay) + "s from now: " + a.label)

    def add_reminder_slot(self, widget, data=None):
        logger.debug("scheduler: add_reminder_slot")
        if data != None:
            self.add_reminder(int(data))

    def expand_inserts(self, note, settings, command = "", sound_file = ""):
        now = datetime.now()
        date_format = date_formats[settings.date_format]
        time_format = time_formats[settings.time_format]

        note = note.replace("+date+", now.strftime(date_format))
        note = note.replace("+month+", now.strftime("%m"))
        note = note.replace("+monthname+", now.strftime("%B"))
        note = note.replace("+day+", now.strftime("%d"))
        note = note.replace("+dayname+", now.strftime("%A"))
        note = note.replace("+yearday+", now.strftime("%j"))
        note = note.replace("+year+", now.strftime("%Y"))

        note = note.replace("+time+", now.strftime(time_format))
        note = note.replace("+hour+", now.strftime("%H"))
        note = note.replace("+hour12+", now.strftime("%I"))
        note = note.replace("+minute+", now.strftime("%M"))
        note = note.replace("+second+", now.strftime("%S"))
        note = note.replace("+microsecond+", now.strftime("%f"))

        note = note.replace("+soundfullfile+", sound_file)
        note = note.replace("+soundfile+", basename(sound_file))
        note = note.replace("+command+", command)

        return note
