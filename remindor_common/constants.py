# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import os

website = "http://bhdouglass.com/remindor/indicator/"
website_qt = "http://bhdouglass.com/remindor/qt/"

blogsite = "http://blog.bhdouglass.com/"
rssfeed = "http://blog.bhdouglass.com/rss"
donatesite = "http://bhdouglass.com/remindor/contribute/"

bugsite = "https://bugs.launchpad.net/indicator-remindor"
bugsite_qt = "https://github.com/bhdouglass/remindor-qt/issues"

featuresite = "https://bugs.launchpad.net/indicator-remindor"
featuresite_qt = "https://github.com/bhdouglass/remindor-qt/issues"

translatesite = "https://translations.launchpad.net/indicator-remindor"

questionsite = "https://answers.launchpad.net/indicator-remindor"
questionsite_qt = "https://github.com/bhdouglass/remindor-qt/issues"

config_path = os.path.expanduser('~') + '/.config/indicator-remindor'
if os.name == 'nt':
    config_path = os.getenv('APPDATA') + '/remindor'

database_file = config_path + '/indicator-remindor.db'
config_file = config_path + '/remindor.conf'
log_file = config_path + '/indicator-remindor.log'
scheduled_file = config_path + '/scheduled-reminders.json'
