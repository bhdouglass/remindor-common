# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import logging
logger = logging.getLogger('remindor_common')

try:
    import dbus
    import dbus.service

    class dbus_service(dbus.service.Object):
        def __init__(self, bus, path='/com/bhdouglass/indicator_remindor/object'):
            dbus.service.Object.__init__(self, bus, path)
            self._bus = bus
            self._path = path
            self._interface = 'com.bhdouglass.indicator_remindor'

        @dbus.service.signal(dbus_interface='com.bhdouglass.indicator_remindor', signature='s')
        def command(self, command):
            pass

        @dbus.service.method(dbus_interface='com.bhdouglass.indicator_remindor')
        def emitUpdate(self):
            self.command('update')
            return 'Signal emitted'

        @dbus.service.method(dbus_interface='com.bhdouglass.indicator_remindor')
        def emitStop(self):
            self.command('stop')
            return 'Signal emitted'

        @dbus.service.method(dbus_interface='com.bhdouglass.indicator_remindor')
        def emitManage(self):
            self.command('manage')
            return 'Signal emitted'

        @dbus.service.method(dbus_interface='com.bhdouglass.indicator_remindor')
        def emitAttention(self):
            self.command('attention')
            return 'Signal emitted'

        @dbus.service.method(dbus_interface='com.bhdouglass.indicator_remindor')
        def emitActive(self):
            self.command('active')
            return 'Signal emitted'

        @dbus.service.method(dbus_interface='com.bhdouglass.indicator_remindor')
        def emitClose(self):
            self.command('close')
            return 'Signal emitted'

        def bus(self):
            return self._bus

        def path(self):
            return self._path

        def interface(self):
            return self._interface

except:
    logger.debug('Unable to initialize dbus in remindor_common.dbus_service, this features will be disabled')
