# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import time, re
from datetime import timedelta, date, datetime, MINYEAR, MAXYEAR
from dateutil import parser

import gettext
from gettext import gettext as _
gettext.textdomain('remindor-common')

internal_time_format = "%H:%M"
internal_date_format = "%m/%d/%Y"

time_formats = ["%I:%M%p", "%H:%M", "%H%M"]
qt_time_formats = ["hh:mmap", "hh:mm", "hhmm"]
date_formats = ["%m/%d/%Y", "%m-%d-%Y", "%m/%d/%y", "%m-%d-%y", "%d/%m/%Y", "%d-%m-%Y", "%d/%m/%y", "%d-%m-%y", "%Y/%m/%d", "%Y-%m-%d", "%y/%m/%d", "%y-%m-%d"]
qt_date_formats = ["MM/dd/yyyy", "MM-dd-yyyy", "MM/dd/yy", "MM-dd-yy", "dd/MM/yyyy", "dd-MM-yyyy", "dd/MM/yy", "dd-MM-yy", "yyyy/MM/dd", "yyyy-MM-dd", "yy/MM/dd", "yy-MM-dd"]

days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
other_days = ["today", "tomorrow"]
other = ["days"]
first_only = ["next", "every"]
second_only = ["weekday", "weekend", "other", "day"]
time_units = ["minute", "hour"]
date_units = ["day", "week", "month", "year"]

def default_datetime():
    return datetime(1900, 1, 1, 0, 0)

def default_date():
    return date(1900, 1, 1)

def default_time():
    return datetime(1900, 1, 1, 0, 0).time()

def time_str_to_datetime(string, time_format = 0):
    if string == "":
        return None
    else:
        try:
            return datetime.strptime(string, time_formats[time_format])
        except:
            return None

def date_str_to_datetime(string, date_format = 0):
    if string == "":
        return None
    else:
        try:
            return datetime.strptime(string, date_formats[date_format])
        except:
            return None

def timestamp(date):
    return time.mktime(date.timetuple())

def whats_first(date_f):
    df = False
    yf = False

    if date_f > 3 and date_f < 8:
        df = True
    elif date_f > 7 and date_f < 12:
        yf = True

    return df, yf

def str_to_date(t, d, offset=0):
    if t is not None and is_time_repeating(t):
        t = str_to_time(t)

    past_time = False
    if offset > 0 and t is not None:
        now_datetime = datetime.now()
        full_t = datetime.strptime(t, internal_time_format)
        if full_t.time() < now_datetime.time():
            past_time = True

    if is_date_repeating(d):
        d = str_to_day(d, offset, past_time).strftime(internal_date_format)

    p = d
    if t is not None:
        p = t + " " + d

    return parser.parse(p)

def str_to_time(t):
    tl = t.split()
    now = datetime.now()

    if len(tl) == 3:
        #ie: every 30 minutes
        #ie: every 1 hours
        midnight = datetime.now()
        midnight = midnight.replace(hour=0, minute=0, second=0, microsecond=0)

        while midnight < now:
            if tl[2] == "minutes":
                delta = timedelta(minutes=int(tl[1]))
            else: #tl[2] == "hours"
                delta = timedelta(hours=int(tl[1]))

            midnight += delta

        return format_internal_time(midnight)

    else: #len(tl) == 7
        #ie: every 30 minutes from 10:30am to 3:00pm
        #ie: every 1 hour from 8:00am to 8:30pm
        start = parser.parse(tl[4])
        end = parser.parse(tl[6])

        if end < now: #end has already passed
            return format_internal_time(end)
        else:
            while start < end and start < now:
                if tl[2] == "minutes":
                    delta = timedelta(minutes=int(tl[1]))
                else: #tl[2] == "hours"
                    delta = timedelta(hours=int(tl[1]))

                start += delta

            return format_internal_time(start)

def str_to_day(d, offset=0, past_time=False):
    dl = d.split()
    today = date.today()
    real_today = date.today()

    if offset > 0:
        delta = timedelta(offset)
        today = today + delta

    #remove to
    end = None
    if len(dl) > 1 and dl[-2] == "to":
        #end = datetime.strptime(dl[-1], internal_date_format).date()
        end = parser.parse(dl[-1]).date() #should be in internal_date_format
        dl.pop(-1) #pop date
        dl.pop(-1) #pop to

    #remove from
    start = None
    if len(dl) > 1 and dl[-2] == "from":
        #start = datetime.strptime(dl[-1], internal_date_format).date()
        start = parser.parse(dl[-1]).date() #should be in internal_date_format
        dl.pop(-1) #pop date
        dl.pop(-1) #pop from

    if start == None:
        start = today

    value = today
    if dl[0] == "every":
        if dl[1] in days: #every monday
            #print "every monday"
            value = datetime.strptime(str_day_to_date(dl[1]), internal_date_format).date()
            if value < start:
                wstart = int(start.strftime("%w"))
                wvalue = int(value.strftime("%w"))

                diff = wvalue - wstart
                if diff < 0:
                    diff += 7 #make it a positive number

                value = start + timedelta(days=diff)

            if value == real_today and past_time:
                value = value + timedelta(days=7)

        elif dl[1] == "day": #every day
            #print "every day"
            value = today
            if value < start:
                value = start

        elif len(dl) == 3 and is_number(dl[1]) and dl[2] == "days": #every X days
            #print "every X days"
            value = start
            day = int(dl[1])
            change = False
            if end == None:
                end = today
                change = True

            while value < today and value < end:
                value += timedelta(days=day)

            if change:
                end = None

        elif is_day_of_month(dl[1]): #every X
            #print "every X"
            '''if today.day == int(dl[1]):
                value = today
            else:
                value = datetime.strptime(str_day_to_date("next " + dl[1]), internal_date_format).date()'''

            change = False
            if end == None:
                end = today.replace(year=(today.year + 1))
                change = True

            value = start
            m = value.month
            if int(dl[1]) < value.day:
                m += 1
                if m > 12:
                    m = 1

            cont = True
            while cont and value < end:
                check = True
                try:
                    value = value.replace(day=int(dl[1]), month=m)
                except:
                    check = False

                if check and value >= today:
                    cont = False

                m += 1
                if m > 12:
                    year = value.year + 1
                    value = value.replace(year=year)
                    m = 1

            if change:
                end = None

            if end != None and value > end:
                value = end

            if value == end and int(dl[1]) != value.day:
                value = today - timedelta(days=1)

        elif dl[1] == "weekday": #every weekday
            #print "every weekday"
            w = int(today.strftime("%w"))
            if w is not 0 and w is not 6: #today is weekday
                value = today
            else: #today is weekend
                value = parser.parse("monday").date()

            if value < start:
                w = int(start.strftime("%w"))
                if w is 0 or w is 6:
                    wstart = int(start.strftime("%w"))
                    diff = 8 - wstart
                    value = start + timedelta(days=diff)
                else:
                    value = start

        elif dl[1] == "weekend": #every weekend
            #print "every weekend"
            w = int(today.strftime("%w"))
            if w is not 0 and w is not 6: #today is weekday
                value = parser.parse("saturday").date()
            else: #today is weekend
                d = today

            if value < start:
                w = int(start.strftime("%w"))
                if w is 0 or w is 6:
                    value = start
                else:
                    wstart = int(start.strftime("%w"))
                    diff = 6 - wstart
                    value = start + timedelta(days=diff)

        elif dl[1] == "other": #every other
            #print "every other"
            value = start
            change = False
            if end == None:
                end = today
                change = True

            while value < today and value < end:
                value += timedelta(days=2)

            if change:
                end = None

            if end != None and value > end:
                value = today - timedelta(days=1)

        else:
            print "error"

    else: #if dl[0] == "next": #next X days
        #print "next X days"
        value = start
        temp = start + timedelta(days=(int(dl[1]) - 1))
        if start <= today and temp > today:
            value = today
        elif start < today:
            value = temp
        #else:
        #    value += timedelta(days=1)

    if end != None and value > end:
        value = end
    #if start != None and value < start:
    #    value = start
    #done on a per item basis

    return value

def format_time(hour, minute, am_pm):
    time_s = ""
    if hour < 10:
        time_s += "0" + str(hour)
    else:
        time_s += str(hour)

    time_s += ":"

    if minute < 10:
        time_s += "0" + str(minute)
    else:
        time_s += str(minute)

    if am_pm is 0:
        time_s += "am"
    else:
        time_s += "pm"

    return time_s

def format_time3(time_o):
    #return format_time2(time_o.strftime("%H"), time_o.strftime("%M"))
    return time_o.strftime("%I:%M%p")

def format_internal_time(time_o):
    return time_o.strftime(internal_time_format)

def unformat_time(time_s):
    colon = time_s.find(":")

    hour = time_s[0:colon]
    minute = time_s[colon + 1: colon + 3]
    am_pm = time_s[-2:]

    return hour, minute, am_pm

def simplify_singular_time(t):
    t = time_to_english(t)

    if t == "" or t == None:
        return None
    elif t == "now":
        t2 = datetime.now()
        return format_internal_time(t2)
    elif t == "noon" or t == "midday":
        noon = datetime.now()
        noon = noon.replace(hour=12, minute=0, second=0, microsecond=0)
        return format_internal_time(noon)
    elif t == "midnight":
        midnight = datetime.now()
        midnight = midnight.replace(hour=0, minute=0, second=0, microsecond=0)
        return format_internal_time(midnight)

    if not time_from_digit(t) == None:
        t = time_from_digit(t)
        #don't return here so t can be parsed later
    else:
        return None

    if t.isalpha(): #for some reason 'a' gets parsed as valid, hence this check
        return None

    try:
        t2 = parser.parse(t, default=default_datetime())

        if not t2.date() == default_date():
            return None

        #time is valid (ie: 10:35am)
        return format_internal_time(t2)
    except ValueError:
        return None
    except TypeError:
        return None

def simplify_repeating_time(t):
    t = time_to_english(t)

    if t == "" or t == None:
        return None

    from_s, to_s, t = get_from_to(t)

    if (to_s == None) ^ (from_s == None):
        #invalid if only one of from/to is present
        return None

    from_t = simplify_singular_time(from_s)
    to_t = simplify_singular_time(to_s)

    if from_t == None and from_s != None:
        #invalid from time
        return None

    if to_t == None and to_s != None:
        #invalid to time
        return None

    if to_s != None:
        from_s = datetime.strptime(from_t, internal_time_format)
        to_s = datetime.strptime(to_t, internal_time_format)

        if from_s >= to_s:
            #from time greater than to time
            return None

    if "minutes" not in t:
        t = t.replace("minute", "minutes")
    if "hours" not in t:
        t = t.replace("hour", "hours")

    t = t.split(' ')

    if t[0] != "every":
        return None
    elif t[-1] != "minutes" and t[-1] != "hours":
        return None

    if len(t) == 3:
        if not is_positive(t[1]):
            return None
    elif len(t) == 2:
        t.insert(1, '1')

    if from_t != None:
        t.append("from")
        t.append(from_t)

        if to_t != None:
            t.append("to")
            t.append(to_t)

    return ' '.join(t)

def str_time_simplify(t):
    t = time_to_english(t)

    if t == "" or t == None:
        return None

    single = simplify_singular_time(t)
    if single != None:
        return single
    else:
        return simplify_repeating_time(t)

def is_time_repeating(t):
    if not time_from_digit(t) == t:
        #it is just a valid digit
        return False
    else:
        try:
            parser.parse(t)
            #it is a valid time format
            return False
        except ValueError:
            #it is a repeating time
            return True
        except TypeError:
            return True

def is_date_repeating(d):
    singular = simplify_singular_date(d, 0)
    if singular == None:
        return True
    else:
        return False

def time_from_digit(t):
    t = str(t)

    if is_number(t):
        t2 = int(t)
        if t2 < 0: #negative number
            return None

        if len(t) == 4 or len(t) == 3:
            if t2 >= 0 and t2 <= 2400:
                min = int(t[-2:])
                if min >= 0 and min < 60:
                    t = t[:-2] + ":" + t[-2:]
                else:
                    return None
            else:
                return None
        elif len(t) == 2 or len(t) == 1:
            if t2 > 0 and t2 <= 24:
                t = t + ":00"
            else:
                return None
        else:
            return None

    return t

def get_from_to(s):
    s = s.split(' ')

    #find from and to
    counter = 0
    to_pos = None
    from_pos = None
    for word in s:
        if word == "from":
            from_pos = counter
        elif word == "to":
            to_pos = counter

        counter += 1

    #extract to
    to_s = None
    if to_pos != None:
        to_s = ' '.join(s[(to_pos + 1):])
        s = s[:to_pos] #remove to

    #extract from
    from_s = None
    if from_pos != None:
        from_s = ' '.join(s[(from_pos + 1):])
        s = s[:(from_pos)] #remove from

    return from_s, to_s, ' '.join(s)

def between_from_to(s, compare):
    if s == None or s == '':
        return False

    (from_s, to_s, rest) = get_from_to(s)

    from_d = None
    to_d = None
    if from_s != None:
        from_d = datetime.strptime(from_s, internal_date_format)

    if to_s != None:
        to_d = datetime.strptime(to_s, internal_date_format)

    return_value = False
    if from_d != None and to_d != None:
        if from_d <= compare and compare <= to_d:
            return_value = True

    if from_d != None and to_d == None:
        if from_d <= compare:
            return_value = True

    if from_d == None and to_d != None:
        if compare <= to_d:
            return_value = True

    return return_value

def simplify_singular_date(d, date_f):
    if d == None or d == "":
        return None

    df, yf = whats_first(date_f)
    d = date_expand(date_to_english(d))
    today = date.today()

    day = str_day_to_date(d)
    if is_valid_timedelta(today, d):
        d2 = today + timedelta(int(d))
        return d2.strftime(internal_date_format)
    elif day != None:
        return day
    else:
        if len(d) == 1 and not is_number(d):
            return None

        d2 = None
        try:
            d2 = parser.parse(d, dayfirst=df, yearfirst=yf, default=default_datetime())

            if not d2.time() == default_time():
                return None

            if d2.year == default_date().year:
                d2 = d2.replace(year=today.year)

        except ValueError:
            return None
        except TypeError:
            return None

        if d2 != None and d2.year >= 1900:
            return d2.strftime(internal_date_format)
        else:
            return None

def date_expand(d):
    d_list = d.split()

    index = 0
    for row in d_list:
        d_list[index] = date_expand_helper(d_list[index])
        index += 1

    return ' '.join(d_list)

def date_expand_helper(d):
    if d == "mon":
        return "monday"
    elif d == "tues":
        return "tuesday"
    elif d == "wed":
        return "wednesday"
    elif d == "thurs":
        return "thursday"
    elif d == "fri":
        return "friday"
    elif d == "sat":
        return "saturday"
    elif d == "sun":
        return "sunday"
    elif d == "tom":
        return "tomorrow"
    elif d == "nx":
        return "next"
    elif d == "ev":
        return "every"
    else:
        return d

def simplify_repeating_date(d, date_f):
    if d == "" or d == None:
        return None

    df, yf = whats_first(date_f)
    d = date_expand(date_to_english(d))
    today = date.today()
    #dl = d.split(" ")

    from_s, to_s, d = get_from_to(d)

    if to_s != None and from_s == None:
        #to but no from
        return None

    from_d = simplify_singular_date(from_s, date_f)
    to_d = simplify_singular_date(to_s, date_f)

    if from_d == None and from_s != None:
        #invalid from date
        return None

    if to_d == None and to_s != None:
        #invalid to date
        return None

    if to_s != None:
        from_s = datetime.strptime(from_d, internal_date_format)
        to_s = datetime.strptime(to_d, internal_date_format)

        if from_s > to_s:
            #from time greater than to time
            return None

    dl = d.split(" ")

    #now d should not have 'from' or 'to' in it
    if len(dl) == 2:
        #every <day>, every <number>, every day, every other, next <number>

        unit = date_unit_replace(dl[1])
        if dl[0] == 'every' and unit in date_units:
            x = 1
            if unit == 'week':
                x = 7
            elif unit == 'month':
                x = 30
            elif unit == 'year':
                x = 365

            dl = ['every', str(x), 'days']
        else:
            if dl[0] not in first_only:
                return None
            elif is_number(dl[1]) and not is_day_of_month(dl[1]):
                #number is not a valid day of the month
                return None
            elif not is_number(dl[1]) and dl[1] not in days and dl[1] not in second_only:
                return None
            elif dl[0] == "next":
                #next <number> is actually a singular date!
                return None
                '''if from_s != None:
                    #can't have from/to with next
                    return None
                elif not is_number(dl[1]):
                    #non number after next
                return None'''

        if d == "every other":
            if from_s == None or from_s == "":
                #add a from date
                from_s = today.strftime(internal_date_format)

    elif len(dl) == 3:
        #every <number> days, next <number> days
        if dl[0] not in first_only:
            return None
        elif not is_number(dl[1]):
            return None

        unit = date_unit_replace(dl[2])
        if unit in date_units:
            x = int(dl[1])
            if unit == "week":
                x = x * 7
            elif unit == "month":
                x = x * 30
            elif unit == "year":
                x = x * 365

            dl[1] = str(x)
            dl[2] = "days"

        if dl[2] != "days" and dl[2] != "day":
            return None
        elif dl[0] == "next" and to_s != None:
            #can't have to with next x days
            return None

        if from_s == None or from_s == "":
            #add a from date
            from_s = today.strftime(internal_date_format)
    else:
        #invalid number of words
        return None

    if from_d != None:
        dl.append("from")
        #dl.append(from_s)
        dl.append(from_d)

        if to_d != None:
            dl.append("to")
            #dl.append(to_s)
            dl.append(to_d)

    return ' '.join(dl)

def str_date_simplify(d, date_f):
    if d == "" or d == None:
        return None

    #df, yf = whats_first(date_f)
    d = date_expand(date_to_english(d))
    #today = date.today()

    singular = simplify_singular_date(d, date_f)
    if singular != None:
        return singular
    else:
        return simplify_repeating_date(d, date_f)

def str_day_to_date(d):
    d = date_expand(date_to_english(d))
    today = date.today()

    if d == "christmas":
        christmas = date(today.year, 12, 25)
        if today > christmas:
            christmas.replace(year=christmas.year + 1)
        return christmas.strftime(internal_date_format)
    elif d == "next christmas":
        christmas = date(today.year + 1, 12, 25)
        return christmas.strftime(internal_date_format)
    elif d == "today":
        return today.strftime(internal_date_format)
    elif d == "tomorrow":
        tom = today + timedelta(1)
        return tom.strftime(internal_date_format)
    if d == "yesterday":
        yest = today + timedelta(-1)
        return yest.strftime(internal_date_format)
    elif d == "monday":
        d2 = parser.parse("monday")
        return d2.strftime(internal_date_format)
    elif d == "tuesday":
        d2 = parser.parse("tuesday")
        return d2.strftime(internal_date_format)
    elif d == "wednesday":
        d2 = parser.parse("wednesday")
        return d2.strftime(internal_date_format)
    elif d == "thursday":
        d2 = parser.parse("thursday")
        return d2.strftime(internal_date_format)
    elif d == "friday":
        d2 = parser.parse("friday")
        return d2.strftime(internal_date_format)
    elif d == "saturday":
        d2 = parser.parse("saturday")
        return d2.strftime(internal_date_format)
    elif d == "sunday":
        d2 = parser.parse("sunday")
        return d2.strftime(internal_date_format)
    elif d == "next monday":
        d2 = parser.parse("monday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    elif d == "next tuesday":
        d2 = parser.parse("tuesday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    elif d == "next wednesday":
        d2 = parser.parse("wednesday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    elif d == "next thursday":
        d2 = parser.parse("thursday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    elif d == "next friday":
        d2 = parser.parse("friday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    elif d == "next saturday":
        d2 = parser.parse("saturday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    elif d == "next sunday":
        d2 = parser.parse("sunday") + timedelta(days=7)
        return d2.strftime(internal_date_format)
    else:
        dl = d.split(' ')
        if len(dl) == 2 and dl[0] == "next" and is_number(dl[1]):
            dom = int(dl[1])
            if is_day_of_month(dom):
                m = today.month
                if dom <= today.day:
                    m += 1
                    if m > 12:
                        m = 1

                d2 = today
                cont = True
                while cont:
                    try:
                        d2 = d2.replace(day=dom, month=m)
                        cont = False
                    except ValueError:
                        m += 1
                        if m > 12:
                            m = 1
                    except TypeError:
                        m += 1
                        if m > 12:
                            m = 1

                return d2.strftime(internal_date_format)
            else:
                return None
        else:
            return None

def fix_time_format(time_s, time_f):
    if time_s == None or time_s == "":
        return None

    time_s2 = time_s
    if not is_time_repeating(time_s):
        time_o = parser.parse(time_s)
        time_s2 = time_o.strftime(time_formats[time_f])
        if time_f == 0:
            time_s2 = format_time3(time_o)
    else:
        tl = time_s.split()
        if len(tl) == 7:
            if tl[4].isdigit() and len(tl[6]) > 2:
                tl[4] = tl[4][:-2] + ":" + tl[4][-2:]
            if tl[6].isdigit() and len(tl[6]) > 2:
                tl[6] = tl[6][:-2] + ":" + tl[6][-2:]

            time_o1 = parser.parse(tl[4])
            time_o2 = parser.parse(tl[6])
            tl[4] = time_o1.strftime(time_formats[time_f])
            tl[6] = time_o2.strftime(time_formats[time_f])
            if time_f == 0:
                tl[4] = format_time3(time_o1)
                tl[6] = format_time3(time_o2)

        time_s2 = " ".join(tl)

    return time_to_local(time_s2)

def fix_date_format(date_s, date_f):
    if date_s == None or date_s == "":
        return None

    date_s2 = date_s
    if not is_date_repeating(date_s):
        date_o = parser.parse(date_s)
        date_s2 = date_o.strftime(date_formats[date_f])
    else:
        from_s, to_s, date_s2 = get_from_to(date_s)
        if from_s != None:
            from_o = parser.parse(from_s)
            date_s2 += " from " + from_o.strftime(date_formats[date_f])

            if to_s != None:
                to_o = parser.parse(to_s)
                date_s2 += " to " + to_o.strftime(date_formats[date_f])

        #dl = date_s.split()
        #if len(dl) == 4 or len(dl) == 5:
        #    date_o = parser.parse(dl[-1])
        #    dl[-1] = date_o.strftime(date_formats[date_f])

        #date_s2 = " ".join(dl)

    return date_to_local(date_s2)

def regex_replace(old, new, string):
    return re.sub(r"\b" + re.escape(old.lower()) + r"\b", new, string)

def date_to_local(d):
    #for translating date from english to local
    #reverse of date_to_english()

    if d == "" or d == None:
        return None

    d = str(d.lower())

    d = d.replace("christmas", _("christmas").lower())
    d = d.replace("today", _("today").lower())
    d = d.replace("yesterday", _("yesterday").lower())
    d = d.replace("tomorrow", _("tomorrow").lower())
    d = d.replace("next", _("next").lower())
    d = d.replace("every day", _("every day").lower())
    d = d.replace("every weekday", _("every weekday").lower())
    d = d.replace("every weekend", _("every weekend").lower())
    d = d.replace("every other", _("every other").lower())
    d = d.replace("every", _("every").lower())
    d = d.replace("day", _("day").lower())
    d = d.replace("days", _("days").lower())
    d = d.replace("week", _("week").lower())
    d = d.replace("weeks", _("weeks").lower())
    d = d.replace("month", _("month").lower())
    d = d.replace("months", _("months").lower())
    d = d.replace("year", _("year").lower())
    d = d.replace("years", _("years").lower())

    d = d.replace("monday", _("monday").lower())
    d = d.replace("tuesday", _("tuesday").lower())
    d = d.replace("wednesday", _("wednesday").lower())
    d = d.replace("thursday", _("thursday").lower())
    d = d.replace("friday", _("friday").lower())
    d = d.replace("saturday", _("saturday").lower())
    d = d.replace("sunday", _("sunday").lower())

    return d

def time_to_local(t):
    #for translating time from english to local
    #reverse of time_to_english()

    if t == "" or t == None:
        return None

    t = str(t.lower())

    t = t.replace("now", _("now").lower())
    t = t.replace("noon", _("noon").lower())
    t = t.replace("midday", _("midday").lower())
    t = t.replace("midnight", _("midnight").lower())
    t = t.replace("every minute", _("every minute").lower())
    t = t.replace("every hour", _("every hour").lower())
    t = t.replace("every", _("every").lower())
    t = t.replace("minutes", _("minutes").lower())
    t = t.replace("hours", _("hours").lower())
    t = t.replace("minute", _("minute").lower())
    t = t.replace("hour", _("hour").lower())

    return t

def date_to_english(d):
    #for translating date from local to english
    #reverse of date_to_local()

    if d == "" or d == None:
        return None

    d = str(d.lower())

    d = d.replace(_("christmas").lower(), "christmas")
    d = d.replace(_("today").lower(), "today")
    d = d.replace(_("yesterday").lower(), "yesterday")
    d = d.replace(_("tomorrow").lower(), "tomorrow")
    #d = d.replace(_("tom").lower(), "tom")
    d = regex_replace(_("tom"), "tom", d)
    d = d.replace(_("next").lower(), "next")
    d = d.replace(_("every day").lower(), "every day")
    d = d.replace(_("every weekday").lower(), "every weekday")
    d = d.replace(_("every weekend").lower(), "every weekend")
    d = d.replace(_("every other").lower(), "every other")
    d = d.replace(_("every").lower(), "every")
    d = regex_replace(_("day").lower(), "day", d)
    d = d.replace(_("days").lower(), "days")
    d = regex_replace(_("week").lower(), "week", d)
    d = d.replace(_("weeks").lower(), "weeks")
    d = regex_replace(_("month").lower(), "month", d)
    d = d.replace(_("months").lower(), "months")
    d = regex_replace(_("year").lower(), "year", d)
    d = d.replace(_("years").lower(), "years")

    d = d.replace(_("monday").lower(), "monday")
    d = d.replace(_("tuesday").lower(), "tuesday")
    d = d.replace(_("wednesday").lower(), "wednesday")
    d = d.replace(_("thursday").lower(), "thursday")
    d = d.replace(_("friday").lower(), "friday")
    d = d.replace(_("saturday").lower(), "saturday")
    d = d.replace(_("sunday").lower(), "sunday")

    #d = d.replace(_("mon").lower(), "mon")
    #d = d.replace(_("tues").lower(), "tues")
    #d = d.replace(_("wed").lower(), "wed")
    #d = d.replace(_("thurs").lower(), "thurs")
    #d = d.replace(_("fri").lower(), "fri")
    #d = d.replace(_("sat").lower(), "sat")
    #d = d.replace(_("sun").lower(), "sun")

    d = regex_replace(_("mon"), "mon", d)
    d = regex_replace(_("tues"), "tues", d)
    d = regex_replace(_("wed"), "wed", d)
    d = regex_replace(_("thurs"), "thurs", d)
    d = regex_replace(_("fri"), "fri", d)
    d = regex_replace(_("sat"), "sat", d)
    d = regex_replace(_("sun"), "sun", d)

    return d

def time_to_english(t):
    #for translating time from local to english
    #reverse of time_to_local()

    if t == "" or t == None:
        return None

    t = str(t.lower())

    t = t.replace(_("now").lower(), "now")
    t = t.replace(_("noon").lower(), "noon")
    t = t.replace(_("midday").lower(), "midday")
    t = t.replace(_("midnight").lower(), "midnight")
    t = t.replace(_("every minute").lower(), "every minute")
    t = t.replace(_("every hour").lower(), "every hour")
    t = t.replace(_("every").lower(), "every")
    t = t.replace(_("minutes").lower(), "minutes")
    t = t.replace(_("hours").lower(), "hours")
    t = t.replace(_("minute").lower(), "minute")
    t = t.replace(_("hour").lower(), "hour")

    return t

def simple_to_local(s):
    #for translating time from english to local
    #reverse of simple_to_english()

    if s == "" or s == None:
        return None

    s = str(s.lower())

    s = s.replace("at", _("at").lower())
    s = s.replace("in", _("in").lower())
    s = s.replace("on", _("on").lower())
    s = s.replace("every", _("every").lower())

    return s

def simple_to_english(s):
    #for translating time from local to english
    #reverse of simple_to_local()

    if s == "" or s == None:
        return None

    s = str(s.lower())

    s = s.replace(_("at").lower(), "at")
    s = s.replace(_("in").lower(), "in")
    s = s.replace(_("on").lower(), "on")
    s = s.replace(_("every").lower(), "every")

    return s

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def is_positive(s):
    if is_number(s):
        if int(s) > 0:
            return True

    return False

def is_day_of_month(s):
    if is_number(s):
        i = int(s)
        if i > 31:
            return False
        elif i < 0:
            return False
        else:
            return True
    else:
        return False

def is_valid_timedelta(d, td):
    if not is_number(td):
        return False
    else:
        check = d.year + (int(td) / 365)
        return check < MAXYEAR and check > MINYEAR

def time_unit_replace(unit):
    unit = unit.replace("minutes", "minute")
    unit = unit.replace("hours", "hour")

    return unit

def date_unit_replace(unit):
    unit = unit.replace("days", "day")
    unit = unit.replace("weeks", "week")
    unit = unit.replace("months", "month")
    unit = unit.replace("years", "year")

    return unit

def simple_parse(s, date_format = 0):
    original = s
    s = s.lower().split()
    today = datetime.now()

    at_pos = None
    in_pos = None
    on_pos = None
    every_pos = None
    counter = 0
    for word in s:
        word = simple_to_english(word)

        if word == "at":
            at_pos = counter
        elif word == "in":
            in_pos = counter
        elif word == "on":
            on_pos = counter
        elif word == "every":
            every_pos = counter

        counter += 1

    date_s = None
    time_s = None
    label = None

    at_s = None
    in_s = None
    on_s = None
    every_s = None

    if at_pos != None:
        at_s = s[(at_pos + 1):]
        at_s = ' '.join(at_s)
        s = s[:at_pos]

        if at_s == '':
            at_s = None

    if in_pos != None:
        in_s = s[(in_pos + 1):]
        in_s = ' '.join(in_s)
        s = s[:in_pos]

        if in_s == '':
            in_s = None

    if on_pos != None:
        on_s = s[(on_pos + 1):]
        on_s = ' '.join(on_s)
        s = s[:on_pos]

        if on_s == '':
            on_s = None

    if every_pos != None:
        every_s = s[every_pos:]
        if len(every_s) > 1:
            every_s = ' '.join(every_s)
            s = s[:(every_pos)]
        else:
            every_s = None

        if every_s == '':
            every_s = None

    #at - singular time (today's date)
    #in - time from now (time and date)
    #on - singular date
    #every - repeating date or repeating time

    nones = 0
    if at_s == None:
        nones += 1
    if in_s == None:
        nones += 1
    if on_s == None:
        nones += 1
    if every_s == None:
        nones += 1

    valid = False
    if nones == 3: #only one part
        valid = True
    elif nones == 2:
        if every_s != None and at_s != None:
            valid = True
        elif on_s != None and at_s != None:
            valid = True

    if valid:
        at_t = simplify_singular_time(at_s)
        (in_d, in_t) = simplify_quick_time(in_s)
        on_t = simplify_singular_date(on_s, date_format)
        every_t = simplify_repeating_date(every_s, date_format)

        every_is_time = False
        if every_t == None and every_s != None:
            every_t = simplify_repeating_time(every_s)
            every_is_time = True

        day_plus_one = False
        if at_s != None and at_t != None:
            time_s = at_t
            time_d = parser.parse(time_s)
            if time_d < today:
                day_plus_one = True

        if in_s != None and in_t != None:
            time_s = in_t
            date_s = in_d
        if on_s != None and on_t != None:
            date_s = on_t
        if every_s != None and every_t != None:
            if every_is_time:
                time_s = every_t
            else:
                date_s = every_t

        if date_s == None and nones == 3 and time_s != None:
            now = datetime.now()
            date_s = now.strftime(internal_date_format)

        if time_s == None and nones == 3 and date_s != None:
            now = datetime.now()
            time_s = now.strftime(internal_time_format)

        if day_plus_one and not is_date_repeating(date_s):
            date_d = parser.parse(date_s)
            now = datetime.now()
            if date_d.date() == now.date():
                now = now + timedelta(days=1)
                date_s = now.strftime(internal_date_format)

        if time_s == None:
            date_s = None
        if date_s == None:
            time_s = None

    if time_s != None and date_s != None:
        label = ' '.join(s)
    else:
        label = original

    return (date_s, time_s, label)

def simplify_quick_time(s):
    date_s = None
    time_s = None

    if s != '' and s != None:
        s = s.lower().split()

        if len(s) == 2:
            time = s[0]
            unit = time_unit_replace(s[1])

            if is_positive(time) and unit in time_units:
                now = datetime.now()
                delta = timedelta(minutes = int(time))
                if unit == "hour":
                    delta = timedelta(hours = int(time))

                now = now + delta
                date_s = now.strftime(internal_date_format)
                time_s = now.strftime(internal_time_format)

    return (date_s, time_s)

def fix_time_from_to(t):
    t2 = time_to_english(t)

    if t2 == "" or t2 == None:
        return None

    from_s, to_s, t3 = get_from_to(t2)
    if (to_s == None) ^ (from_s == None):
        #missing either from or to
        if to_s == None:
            return time_to_local(t + " to 23:59")
        if from_s == None:
            return t3
