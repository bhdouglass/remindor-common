# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import requests
import sys

import logging
logger = logging.getLogger('remindor_common')

from remindor_common.services.post import Post

class Boxcar():
    api_url = 'https://new.boxcar.io/api/notifications'

    def __init__(self, token):
        self.token = token

    def notify(self, title, message, callback = None, err_callback = None):
        data = {'user_credentials': self.token, 'notification[title]': title,
                'notification[long_message]': message, 'notification[source_name]': 'Remindor'}
        b = Post(self.api_url, data)
        b.start()
