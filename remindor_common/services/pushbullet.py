# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import requests
import sys
import json

import logging
logger = logging.getLogger('remindor_common')

from remindor_common.services.post import Post

class PushbulletException(Exception):
    def __init__(self, status, message = ''):
        self.status = status

        if message == '':
            self.message = ''
            if status == -1:
                self.message = 'Error making request'
            elif status == 400:
                self.message = 'Bad Request'
            elif status == 401:
                self.message = 'Invalid api key provided'
            elif status == 402:
                self.message = 'Request Failed'
            elif status == 403:
                self.message = 'Invalid api key for this request'
            elif status == 404:
                self.message = 'Not found'
            elif status >= 500 and status < 600:
                self.message = 'Pushbullet Server error'
            else:
                self.message = 'Unkown error'
        else:
            self.message = message

        logger.debug('Pushbullet Exception: %s (%s)' % (self.message, self.status))

    def __str__(self):
        return repr('%s (%s)' % (self.message, self.status))

class Pushbullet():
    api_base = 'https://api.pushbullet.com/v2/'
    api_key = ''
    api_auth = ('', '')

    def __init__(self, api_key):
        self.api_key = api_key
        self.api_auth = (self.api_key, '')

    def devices(self):
        devices = []
        status = -1

        try:
            r = requests.get(self.api_base + 'devices', auth=self.api_auth)
            status = r.status_code
        except:
            logger.debug("exception caught trying get device list: " + str(sys.exc_info()[0]))

        if status == 200:
            #response = r.json()
            #old versions of requests didn't have the json method
            response = json.loads(r.content)

            for device in response['devices']:
                if device['pushable']:
                    devices.append({
                        'id': device['iden'],
                        'name': device['nickname']
                    })
        else:
            raise PushbulletException(status)

        return devices

    def push(self, device, type, param1, param2 = None, callback = None, err_callback = None):
        data = {
            'device_iden': device,
            'type': type
        }
        files = None

        if type == 'note':
            data['title'] = param1
            data['body'] = param2
        elif type == 'link':
            data['title'] = param1
            data['url'] = param2
        elif type == 'address':
            data['name'] = param1
            data['address'] = param2
        elif type == 'list':
            data['title'] = param1
            data['list'] = param2
        #elif type == 'file':
        #    files = {file: open(param1)}
        else:
            raise PushbulletException(-1, 'Invalid push type specified')

        p = Post(self.api_base + 'pushes', data=data, auth=self.api_auth, files=files, callback=callback, err_callback=err_callback, use_params=False)
        p.start()

    def note(self, device, title, body):
        self.push(device, 'note', title, body)

    def link(self, device, title, url):
        self.push(device, 'link', title, url)

    def address(self, device, name, address):
        self.push(device, 'address', name, address)

    def list(self, device, title, list):
        self.push(device, 'list', title, list)

    #def file(self, device, file):
        #self.push(device, 'file', file)
