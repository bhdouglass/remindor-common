# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2013 Brian Douglass bhdouglass@gmail.com
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranties of
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import threading
import sys
import requests

import logging
logger = logging.getLogger('remindor_common')

class Post(threading.Thread):
    def __init__(self, url, data = None, auth = None, files = None, callback =  None, err_callback = None, use_params=True):
        threading.Thread.__init__(self)
        self.url = url
        self.data = data
        self.auth = auth
        self.files = files
        self.callback = callback
        self.err_callback = err_callback
        self.use_params = use_params

        self.headers = {}
        if requests.__version__ < '1.1.0':
            self.headers = {'Content-Length': '0'} #for old versions of requests

    def run(self):
        status = -1
        r = None
        logger.debug("sending post request to: " + self.url)
        try:
            r = None
            if self.use_params:
                r = requests.post(self.url, params=self.data, auth=self.auth, files=self.files, headers=self.headers)
            else:
                r = requests.post(self.url, data=self.data, auth=self.auth, files=self.files, headers=self.headers)

            logger.debug("Post request content: " + r.content)
            status = r.status_code
        except:
            logger.warning("Exception caught trying post request: " + str(sys.exc_info()[0]))

        logger.debug("Post request status: " + str(status))

        if status < 200 and status > 202:
            if self.err_callback is None:
                raise Exception('%d response recieved while posting to %s' % (status, self.url))
            else:
                self.err_callback(status)
        elif self.callback is not None:
            self.callback(r)
